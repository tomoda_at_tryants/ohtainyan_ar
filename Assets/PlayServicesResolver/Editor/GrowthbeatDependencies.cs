﻿using System;
using System.Collections.Generic;
using UnityEditor;

[InitializeOnLoad]
public class GrowthbeatSampleDependencies : AssetPostprocessor {

        #if UNITY_ANDROID
        public static object svcSupport;
        #endif

        static GrowthbeatSampleDependencies() {
                RegisterDependencies();
        }

        public static void RegisterDependencies() {
                #if UNITY_ANDROID
                RegisterAndroidDependencies();
                #elif UNITY_IOS
                RegisterIOSDependencies();
                #endif
        }

        public static void RegisterAndroidDependencies() {
                #if UNITY_ANDROID
                Type playServicesSupport = Google.VersionHandler.FindClass(
                        "Google.JarResolver", "Google.JarResolver.PlayServicesSupport");
                if (playServicesSupport == null) {
                        return;
                }

                svcSupport = svcSupport ?? Google.VersionHandler.InvokeStaticMethod(
                        playServicesSupport, "CreateInstance",
                        new object[] {
                                "GrowthbeatSampleDependencies",
                                EditorPrefs.GetString("AndroidSdkRoot"),
                                "ProjectSettings"
                        });

                // play-services-gcmの依存解決
                // object[]の3番目は使用したいバージョンを指定してください
                Google.VersionHandler.InvokeInstanceMethod(
                        svcSupport, "DependOn",
                        new object[] {
                                "com.google.android.gms",
                                "play-services-gcm",
                                "8.3.+" },
                        namedArgs: new Dictionary<string, object>() {
                                {"packageIds", new string[] { "extra-google-m2repository" } }
                        });

                // play-services-adsの依存解決
                // object[]の3番目は使用したいバージョンを指定してください
                Google.VersionHandler.InvokeInstanceMethod(
                        svcSupport, "DependOn",
                        new object[] {
                                "com.google.android.gms",
                                "play-services-ads",
                                "8.3.+" },
                        namedArgs: new Dictionary<string, object>() {
                                {"packageIds", new string[] { "extra-google-m2repository" } }
                        });

                // supportライブラリの依存解決
                // object[]の3番目は使用したいバージョンを指定してください
                Google.VersionHandler.InvokeInstanceMethod(
                        svcSupport, "DependOn",
                        new object[] { "com.android.support", "support-v4", "23.0.+" },
                        namedArgs: new Dictionary<string, object>() {
                                {"packageIds", new string[] { "extra-android-m2repository" } }
                        });
                #endif
        }

        public static void RegisterIOSDependencies() {
#if false
                Type iosResolver = Google.VersionHandler.FindClass(
                "Google.IOSResolver", "Google.IOSResolver");
                if (iosResolver == null) {
                return;
                }

                // 今回はiOSの依存解決は使用しない

#endif
        }

        // Handle delayed loading of the dependency resolvers.
        private static void OnPostprocessAllAssets(
                string[] importedAssets, string[] deletedAssets,
                string[] movedAssets, string[] movedFromPath) {
                foreach (string asset in importedAssets) {
                        if (asset.Contains("IOSResolver") ||
                                asset.Contains("JarResolver")) {
                                RegisterDependencies();
                                break;
                        }
                }
        }

}