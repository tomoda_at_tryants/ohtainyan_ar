﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class SmartAREffector : MonoBehaviour
{
    [HideInInspector]
    public string[] targetID = null;

    [HideInInspector]
    public smartar.RecognitionResult result_;
    [HideInInspector]
    public float nearClipPlane_;

    [SerializeField]
    public Camera _SmartARCamera;

    private SmartARController smartARController_;
    private IntPtr landmarkBuffer_ = IntPtr.Zero;
    private IntPtr nodePointBuffer_ = IntPtr.Zero;
    private IntPtr initPointBuffer_ = IntPtr.Zero;

    private TransformData cameraTransform_ = new TransformData();

    private struct TransformData
    {
        public Vector3 position;
        public Quaternion rotation;

        public Vector3 localPosition;
        public Vector3 localScale;
        public Quaternion localRotation;

        public void copyData(Transform transform)
        {
            this.position = transform.position;
            this.rotation = transform.rotation;
            this.localPosition = transform.localPosition;
            this.localScale = transform.localScale;
            this.localRotation = transform.localRotation;
        }
    }

    /// <summary>
    /// 子オブジェクト
    /// </summary>
    /// <value>The child.</value>
    public GameObject       child;

    bool m_bBeginTracking;
    /// <summary>
    /// 認識開始
    /// </summary>
    /// <value><c>true</c> if begin tracking; otherwise, <c>false</c>.</value>
    public bool beginTracking
    {
        get{ return m_bBeginTracking; }
        set{ m_bBeginTracking = value; }
    }

    public bool isTracking
    {
        get
        {
            return  (result_.isRecognized_) ? true : false;
        }
    }

	private void DoCreate() {
		if (landmarkBuffer_ != IntPtr.Zero) {
			return;
		}

        //  ターゲットIDの初期化
        int a_nIndex        = 0;
        int a_nDataCount    = def.instance.configTable.dataCount;
        if (a_nDataCount > 0)
        {
            targetID = new string[a_nDataCount];
            foreach (ConfigTable.Data a_Data in def.instance.configTable.dataList)
            {
                targetID[a_nIndex] = a_Data.targetID;
            }

            a_nIndex++;
        }
		
		landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
		nodePointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.NodePoint)) * smartar.Recognizer.MAX_NUM_NODE_POINTS);
		initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);
	}
	
	private void DoEnable() {
		if (smartARController_ != null) {
			return;
		}

		// Find SmartARController
		var controllers = (SmartARController[]) FindObjectsOfType(typeof(SmartARController));
		if (controllers != null && controllers.Length > 0) {
			smartARController_ = controllers[0];
		}

        if (smartARController_.smart_.isConstructorFailed()) { return; }

        bool active = false;
        foreach (var target in smartARController_.recognizerSettings_.targets)
        {
//  TargetIDを単一で管理している時
#if false
            if (targetID == target.id) active = true;
#else
            foreach (string a_nTargetID in targetID)
            {
                if (a_nTargetID == target.id)
                {
                    active = true;
                    break;
                }
            }
#endif
        }

        gameObject.SetActive(active);

		// For scene mapping mode which not use targets
		bool isRecognittionModeSceneMapping = 
			smartARController_.recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING;
		bool isSceneMappingInitModeTarget =
			smartARController_.recognizerSettings_.sceneMappingInitMode == smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET;
		if (isRecognittionModeSceneMapping && !isSceneMappingInitModeTarget) {
			targetID = null;
		}
	}
	
	private void DoDisable() {
	}
	
	private void DoDestroy() {
		if (landmarkBuffer_ == IntPtr.Zero) {
			return;
		}
		
		Marshal.FreeCoTaskMem(landmarkBuffer_);
		landmarkBuffer_ = IntPtr.Zero;
		Marshal.FreeCoTaskMem(nodePointBuffer_);
		nodePointBuffer_ = IntPtr.Zero;
		Marshal.FreeCoTaskMem(initPointBuffer_);
		initPointBuffer_ = IntPtr.Zero;
	}

	void Awake () {
		DoCreate();
	}
	
	void Start () {
		DoEnable();
		showOrHideChildren(false);
		nearClipPlane_ = Camera.main.nearClipPlane * 1000;
	}
	
	void OnEnable () {
	}
	
	void OnDisable () {
		DoDisable();
	}
	
	void OnDestroy () {
		DoDisable();
		DoDestroy();
	}
	
	void OnApplicationFocus (bool focus) {
	}
	
	void OnApplicationPause (bool pause) {
		DoDisable();
	}
	
	void OnApplicationQuit () {
		DoDisable();
		DoDestroy();
	}
	
	void Update ()
	{
		if (smartARController_ == null)
		{
			return;
		}

		if (!smartARController_.enabled_)
		{
			return;
		}

		if (smartARController_.smart_.isConstructorFailed()) { return; }

		if (smartARController_.isLoadSceneMap_ && targetID != null)
		{
            //  TargetIDを単一で管理している時
#if false
            gameObject.SetActive(false);
            return;
#else
            //  TargetIDを配列で管理している時
            foreach (string a_TargetID in targetID)
            {
                gameObject.SetActive(false);
                return;
            }
#endif
            targetID = null;
		}

		// Get recognition result
	    result_ 				= new smartar.RecognitionResult();
	    result_.maxLandmarks_	= smartar.Recognizer.MAX_NUM_LANDMARKS;
	    result_.landmarks_		= landmarkBuffer_;
		result_.maxNodePoints_	= smartar.Recognizer.MAX_NUM_NODE_POINTS;
		result_.nodePoints_		= nodePointBuffer_;
	    result_.maxInitPoints_ 	= smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS;
	    result_.initPoints_ 	= initPointBuffer_;

        //  TargetIDを単一で管理している時
#if false
        smartARController_.GetResult(targetID, ref result_);
#else
        //  TargetIDを配列で管理している時
        //  認識結果を取得
        foreach (string a_TargetID in targetID)
        {
            smartARController_.GetResult(a_TargetID, ref result_);
            if (result_.isRecognized_)
            {
                break;
            }
        }
#endif

        //  認識結果によって表示の設定を行う
        showOrHideChildren(result_.isRecognized_);
	    setPose(transform, result_);
	}

	void OnGUI()
    {
		DoEnable();
		showOrHideChildren(false);
	}

	public void setPose(Transform transformObject, smartar.RecognitionResult result)
	{
		smartar.Vector3 rotPosition;
		smartar.Quaternion rotRotation;
		smartARController_.adjustPose(result.position_, result.rotation_, out rotPosition, out rotRotation);

		scn_main.s_RotPosition = result.position_;
        scn_main.s_RotRotation = result.rotation_;

	    cameraTransform_.copyData(_SmartARCamera.transform);

		// Set pose
		if (result.isRecognized_)
		{
			transformObject.position = new Vector3(
				cameraTransform_.position.x - (rotPosition.x_ * nearClipPlane_),
				cameraTransform_.position.z - (rotPosition.z_ * nearClipPlane_),
				cameraTransform_.position.y - (rotPosition.y_ * nearClipPlane_)
			);

            transformObject.rotation = Quaternion.identity;
			Quaternion q = new Quaternion(
				-rotRotation.x_,
				-rotRotation.z_,
				-rotRotation.y_,
				 rotRotation.w_
			);

            		float angle;
			Vector3 axis;
			q.ToAngleAxis(out angle, out axis);
			transformObject.RotateAround(cameraTransform_.position, axis, angle);
			transformObject.RotateAround(cameraTransform_.position, Vector3.right, 90);
			transformObject.RotateAround(cameraTransform_.position, Vector3.up, cameraTransform_.rotation.eulerAngles.y);
        }
	}

	private void showOrHideChildren(bool enabled)
	{
		var renderers = GetComponentsInChildren<Renderer>();
		foreach (var renderer in renderers)
		{
			renderer.enabled = enabled;
		}
		var colliders = GetComponentsInChildren<Collider>();
		foreach (var collider in colliders)
		{
			collider.enabled = enabled;
		}
	}
}
