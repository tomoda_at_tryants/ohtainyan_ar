﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;

public class SmartARController : MonoBehaviour {
	[System.Serializable]
	public class TargetEntry {
		public string fileName;
		public string id;
	}

	[System.Serializable]
	public class RecognizerSettings {
		public smartar.RecognitionMode recognitionMode = smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING;
		public smartar.SceneMappingInitMode sceneMappingInitMode = smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET;
		public TargetEntry[] targets = {};
		public int maxTargetsPerFrame = 1;
		public smartar.SearchPolicy searchPolicy = smartar.SearchPolicy.SEARCH_POLICY_PRECISIVE;
		public int maxTriangulateMasks = 0;
		public smartar.DenseMapMode denseMapMode = smartar.DenseMapMode.DENSE_MAP_DISABLE;

		public bool useWorkerThread = true;
	}

	[System.Serializable]
	public class CameraDeviceSettings {
		public int cameraId = 0;
		public Vector2 videoImageSize = new Vector2(0, 0);
		public smartar.FocusMode focusMode = smartar.FocusMode.FOCUS_MODE_CONTINUOUS_AUTO_VIDEO;
	}

	[System.Serializable]
	public class MiscSettings {
		public bool showLandmarks = false;
		public bool showCameraPreview = true;
	}

	public string licenseFileName;

	public CaptureManager	captureManager;

	// Fields configurable on inspector
	[SerializeField]
	private RecognizerSettings recognizerSettings = new RecognizerSettings();
	[SerializeField]
	private CameraDeviceSettings cameraDeviceSettings = new CameraDeviceSettings();
	[SerializeField]
	private MiscSettings miscSettings = new MiscSettings();

	// Public properties for runtime configuration
	public RecognizerSettings recognizerSettings_ {
		get {
			return recognizerSettings;
		}
		set {
			ConfigRecognizer(value, false);
		}
	}

	public CameraDeviceSettings cameraDeviceSettings_ {
		get {
			return cameraDeviceSettings;
		}
		set {
			ConfigCameraDevice(value, false);
		}
	}

	public MiscSettings miscSettings_ {
		get {
			return miscSettings;
		}
		set {
			ConfigMisc(value, false);
		}
	}

	public smartar.Triangle2[] triangulateMasks_ {
		set {
			sarSmartar_SarSmartARController_sarSetTriangulateMasks(self_, value, value.Length);
		}
	}


	private IntPtr self_;

	private bool isRecognized_ = false;
	public bool isLastRecognized() {
		return isRecognized_;
	}

	// Fields
	[HideInInspector]
	public smartar.Smart smart_;
	[HideInInspector]
	public smartar.CameraDevice cameraDevice_;
	[HideInInspector]
	public smartar.SensorDevice sensorDevice_;
	[HideInInspector]
	public smartar.ScreenDevice screenDevice_;
	[HideInInspector]
	public smartar.CameraImageDrawer cameraImageDrawer_;
	[HideInInspector]
	public smartar.LandmarkDrawer landmarkDrawer_;
	[HideInInspector]
	public smartar.Recognizer recognizer_;
	[HideInInspector]
	public smartar.Target[] targets_;
	[HideInInspector]
	public bool isFlipX_ = false;
	[HideInInspector]
	public bool isFlipY_ = false;
	[HideInInspector]
	public smartar.Rotation cameraRotation_;

	private int numWorkerThreads_;

	private Thread[] workerThreads_;

	private bool isFront_ = false;
	private bool started_ = false;
	private bool smartInitFailed_ = false;
	[HideInInspector]
	public bool enabled_ = false;

	[HideInInspector]
	public bool isLoadSceneMap_ = false;
	[HideInInspector]
	private string sceneMapFilePath_;

	// Fields for LandmarkDrawer
	private IntPtr landmarkBuffer_ = IntPtr.Zero;
	private IntPtr nodePointBuffer_ = IntPtr.Zero;
	private IntPtr initPointBuffer_ = IntPtr.Zero;

	// Fields for development
	private float drawTimeSec_ = 0.0f;
	private float drawStartTimeSec_;
	private ulong[] recogCountBuf_;
	private ulong[] recogTimeBuf_;

	#if UNITY_STANDALONE_WIN || UNITY_EDITOR
	private enum RenderEventID
	{
		DoDraw = 3001,
	}
	#endif

	// Properties for development
	public ulong cameraFrameCount_ {
		get {
			return sarSmartar_SarSmartARController_sarGetCameraFrameCount(self_);
		}
	}

	public ulong[] recogCount_ {
		get {
			for (int i = 0; i < recogCountBuf_.Length; ++i) {
				recogCountBuf_[i] = sarSmartar_SarSmartARController_sarGetRecogCount(self_, i);
			}
			return recogCountBuf_;
		}
	}

	public ulong[] recogTime_ {
		get {
			for (int i = 0; i < recogTimeBuf_.Length; ++i) {
				recogTimeBuf_[i] = sarSmartar_SarSmartARController_sarGetRecogTime(self_, i);
			}
			return recogTimeBuf_;
		}
	}

	public ulong drawCount_ {
		get {
			return (ulong) Time.renderedFrameCount;
		}
	}

	public ulong drawTime_ {
		get {
			return (ulong)(drawTimeSec_ * 1000);
		}
	}

	private bool existsStreamingAsset(string fileName)
	{
		var path = Path.Combine(Application.streamingAssetsPath, fileName);
#if UNITY_ANDROID && !UNITY_EDITOR
		Debug.Log("パス：" + path);
		using (var www = new WWW(path))
		{
			while (!www.isDone) {}
			return	www.size > 0;
		}
#else
		return File.Exists(path);
#endif
	}

	private void ConfigRecognizer (RecognizerSettings newSettings, bool creating)
	{
		if (creating || newSettings.maxTargetsPerFrame != recognizerSettings.maxTargetsPerFrame)
        {
			recognizer_.SetMaxTargetsPerFrame(newSettings.maxTargetsPerFrame);
		}

        if (creating || newSettings.searchPolicy != recognizerSettings.searchPolicy)
        {
			recognizer_.SetSearchPolicy(newSettings.searchPolicy);
		}

        if (creating || newSettings.maxTriangulateMasks != recognizerSettings.maxTriangulateMasks)
        {
			recognizer_.SetMaxTriangulateMasks(newSettings.maxTriangulateMasks);
		}

        if (creating || newSettings.denseMapMode != recognizerSettings.denseMapMode)
        {
			recognizer_.SetDenseMapMode(newSettings.denseMapMode);
		}

        if (creating || !Enumerable.SequenceEqual(newSettings.targets, recognizerSettings.targets))
		{
			var oldTargets = targets_;

			targets_ = new smartar.Target[newSettings.targets.Length];
			for (int i = 0, num = newSettings.targets.Length; i < num; ++i)
			{
				string name = newSettings.targets[i].fileName;
				if (isLoadSceneMap_)
				{
					var stream = new smartar.FileStreamIn(smart_, sceneMapFilePath_);
					targets_[i] = new smartar.SceneMapTarget(smart_, stream);
					stream.Close();
				}
				else
				{
					if (name.EndsWith(".v9"))
					{
						name += ".dic";
					}
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
					name = Application.streamingAssetsPath + "/" + name;
#endif
					if (!existsStreamingAsset(name))
					{
						targets_ = null;
						if (newSettings.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING
							|| (newSettings.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING
								&& newSettings.sceneMappingInitMode == smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET))
						{
							// TODO: show message
						}

						return;
					}

					smartar.AssetStreamIn stream = new smartar.AssetStreamIn(smart_, name);
                    if (name.EndsWith(".map", StringComparison.OrdinalIgnoreCase))
					{
                        targets_[i] = new smartar.SceneMapTarget(smart_, stream);
                    }
					else
					{
                        targets_[i] = new smartar.LearnedImageTarget(smart_, stream);
                    }

                    stream.Close();
                }
			}

            recognizer_.SetTargets(targets_);
            
            if (oldTargets != null)
			{
                for (int i = 0, num = oldTargets.Length; i < num; ++i)
				{
                    ((IDisposable)oldTargets[i]).Dispose();
				}
			}
		}

		recognizerSettings = newSettings;
	}

	private void ConfigCameraDevice (CameraDeviceSettings newSettings, bool creating)
	{
		if (!creating && (newSettings.cameraId != cameraDeviceSettings.cameraId
			|| newSettings.videoImageSize != cameraDeviceSettings.videoImageSize)) {
			cameraDeviceSettings = newSettings;
			if (enabled_) {
				DoDisable();
				DoEnable();
			}
		} else {
			if (creating || newSettings.focusMode != cameraDeviceSettings.focusMode) {
				var modes = new smartar.FocusMode[32];
				var numModes = cameraDevice_.GetSupportedFocusMode(modes);
				modes = new ArraySegment<smartar.FocusMode>(modes, 0, numModes).Array;
				if (((System.Collections.Generic.IList<smartar.FocusMode>)modes).Contains(newSettings.focusMode)) {
					cameraDevice_.SetFocusMode(newSettings.focusMode);
				}
			}

			cameraDeviceSettings = newSettings;
		}
	}

	private void ConfigMisc (MiscSettings newSettings, bool creating)
	{
		miscSettings = newSettings;
	}

	private void DoCreate() {
		if (started_) {
			return;
		}

        //  ターゲットデータの初期化
        if (recognizerSettings.targets == null || recognizerSettings.targets.Length == 0)
        {
            int a_nIndex = 0;
            int a_nDataCount = def.instance.configTable.dataCount;
            if (a_nDataCount > 0)
            {
                recognizerSettings.targets = new TargetEntry[a_nDataCount];

                foreach (ConfigTable.Data a_Data in def.instance.configTable.dataList)
                {
                    recognizerSettings.targets[a_nIndex] = new TargetEntry();

                    recognizerSettings.targets[a_nIndex].fileName   = a_Data.dicName;
                    recognizerSettings.targets[a_nIndex].id         = a_Data.targetID;

                    a_nIndex++;
                }
            }
        }

        // Create Smart
        smart_ = new smartar.Smart(licenseFileName);
		smartInitFailed_ = smart_.isConstructorFailed();

		if (smartInitFailed_) { return; }

		// Create CameraImageDrawer
		cameraImageDrawer_ = new smartar.CameraImageDrawer(smart_);

		// Create LandmarkDrawer
		landmarkDrawer_ = new smartar.LandmarkDrawer(smart_);

		// Create Recognizer
		recognizer_ = new smartar.Recognizer(smart_, recognizerSettings.recognitionMode, recognizerSettings.sceneMappingInitMode);
		ConfigRecognizer(recognizerSettings, true);

		sensorDevice_ = new smartar.SensorDevice(smart_);

		smartar.SensorDeviceInfo sensorInfo = new smartar.SensorDeviceInfo();
		sensorDevice_.GetDeviceInfo(sensorInfo);
		recognizer_.SetSensorDeviceInfo(sensorInfo);

		// Create ScreenDevice
		screenDevice_ = new smartar.ScreenDevice(smart_);

		// Create native instance
		if (smartar.Utility.isMultiCore() && recognizerSettings.useWorkerThread ) {
			bool isTargetTracking = recognizerSettings.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_TARGET_TRACKING;
			numWorkerThreads_ = isTargetTracking ? 2 : 1;
		} else {
			numWorkerThreads_ = 0;
		}
		CreateParam param;
		param.smart_ = smart_.self_;
		param.sensorDevice_ = sensorDevice_.self_;
		param.screenDevice_ = screenDevice_.self_;
		param.recognizer_ = recognizer_.self_;
		param.cameraImageDrawer_ = cameraImageDrawer_.self_;
		self_ = sarSmartar_SarSmartARController_sarDoCreate(ref param, numWorkerThreads_ > 0);

		// Init fields for LandmarkDrawer
		landmarkBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.Landmark)) * smartar.Recognizer.MAX_NUM_LANDMARKS);
		nodePointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.NodePoint)) * smartar.Recognizer.MAX_NUM_NODE_POINTS);
		initPointBuffer_ = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(smartar.InitPoint)) * smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS);

		// Init fields for development
		recogCountBuf_ = new ulong[numWorkerThreads_];
		recogTimeBuf_ = new ulong[numWorkerThreads_];

		// Start worker threads
		if (numWorkerThreads_ > 0) {
			workerThreads_ = new Thread[numWorkerThreads_];
			for (int i = 0; i < numWorkerThreads_; ++i) {
				Thread thread = new Thread(() => {
					sarSmartar_SarSmartARController_sarRunWorkerThread(self_);
				});
				thread.Start();
				workerThreads_[i] = thread;
			}
		} else {
			workerThreads_ = null;
		}

		started_ = true;
	}

	private void DoEnable() {
		if (enabled_) {
			return;
		}

		if (smartInitFailed_) { return; }

		// Create CameraDevice
		cameraDevice_ = new smartar.CameraDevice(smart_, cameraDeviceSettings.cameraId);

		cameraDevice_.GetRotation(out cameraRotation_);
		int frontCameraId;
		smartar.CameraDevice.GetDefaultCameraId(smart_, smartar.Facing.FACING_FRONT, out frontCameraId);
		isFront_ = frontCameraId != smartar.CameraDevice.INVALID_CAMERA_ID && cameraDeviceSettings.cameraId == frontCameraId;
		//cameraDevice_.SetVideoImageListener(new MyVideoImageListener(this));
		if (cameraDeviceSettings.videoImageSize.x != 0 && cameraDeviceSettings.videoImageSize.y != 0) {
			cameraDevice_.SetVideoImageSize((int) cameraDeviceSettings.videoImageSize.x, (int) cameraDeviceSettings.videoImageSize.y);
		}
		ConfigCameraDevice(cameraDeviceSettings, true);

		isFlipX_ = false;
		isFlipY_ = false;
		if (isFront_) {
#if UNITY_IPHONE && !UNITY_EDITOR
			isFlipY_ = true;
#elif UNITY_EDITOR || UNITY_STANDALONE_WIN
			// NOOP
#else
			if (cameraRotation_ == smartar.Rotation.ROTATION_0 || cameraRotation_ == smartar.Rotation.ROTATION_90) {
			isFlipX_ = true;
			} else {
			isFlipY_ = true;
			}
#endif
		}
		cameraImageDrawer_.SetFlipX(isFlipX_);
		cameraImageDrawer_.SetFlipY(isFlipY_);

		// Setup Recognizer
		smartar.CameraDeviceInfo cameraInfo = new smartar.CameraDeviceInfo();
		cameraDevice_.GetDeviceInfo(cameraInfo);
		recognizer_.SetCameraDeviceInfo(cameraInfo);

		// Enable native instance
		sarSmartar_SarSmartARController_sarDoEnable(self_, cameraDevice_.self_);

		// Start components
		cameraImageDrawer_.Start();
		landmarkDrawer_.Start();
		cameraDevice_.Start();
		sensorDevice_.Start();

		if (GetComponent<Camera>().clearFlags != CameraClearFlags.Depth && GetComponent<Camera>().clearFlags != CameraClearFlags.Nothing) {
			GetComponent<Camera>().clearFlags = CameraClearFlags.Depth;
		}

		enabled_ = true;
	}

	private void DoDisable() {
		if (!enabled_) {
			return;
		}
		enabled_ = false;

		if (smartInitFailed_) { return; }

		// Stop components
		sensorDevice_.Stop();
		cameraDevice_.Stop();
		landmarkDrawer_.Stop();
		cameraImageDrawer_.Stop();
		recognizer_.Reset();

		// Disable native instance
		sarSmartar_SarSmartARController_sarDoDisable(self_);

		// Dispose CameraDevice
		cameraDevice_.Dispose();
	}

	private void DoDestroy() {
		if (!started_) {
			return;
		}
		started_ = false;

		if (smartInitFailed_) { return; }

		// Finish worker threads
		if (numWorkerThreads_ > 0) {
			sarSmartar_SarSmartARController_sarFinishWorkerThread(self_);
			for (int i = 0; i < numWorkerThreads_; ++i) {
				workerThreads_[i].Join();
			}
		}

		// clear dics
		recognizer_.SetTargets(null);

		// Free fields for LandmarkDrawer
		Marshal.FreeCoTaskMem(landmarkBuffer_);
		landmarkBuffer_ = IntPtr.Zero;
		Marshal.FreeCoTaskMem(nodePointBuffer_);
		nodePointBuffer_ = IntPtr.Zero;
		Marshal.FreeCoTaskMem(initPointBuffer_);
		initPointBuffer_ = IntPtr.Zero;

		// Destroy native instance
		sarSmartar_SarSmartARController_sarDoDestroy(self_);
		self_ = IntPtr.Zero;

		screenDevice_.Dispose();

		sensorDevice_.Dispose();

		recognizer_.Dispose();

		if (targets_ != null)
		{
			for (int i = 0, num = targets_.Length; i < num; ++i)
			{
				((IDisposable)targets_[i]).Dispose();
			}
		}

		cameraImageDrawer_.Dispose();
		landmarkDrawer_.Dispose();

		smart_.Dispose();
	}

	public void resetController()
	{
		cameraDevice_.Stop();
		recognizer_.Reset();
		cameraDevice_.Start();
	}

	public void restartController() {
		DoDisable();
		DoDestroy();
		DoCreate();
	}

	public void reCreateController(
		smartar.RecognitionMode recognitionMode,
		smartar.SceneMappingInitMode sceneMappingInitMode = smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET)
	{
		DoDisable();
		DoDestroy();
		recognizerSettings.recognitionMode = recognitionMode;
		recognizerSettings.sceneMappingInitMode = sceneMappingInitMode;
		DoCreate();
	}

	public void loadSceneMap(string filePath)
	{
		if (!File.Exists(filePath)) { return; }

		recognizerSettings.recognitionMode = smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING;
		recognizerSettings.sceneMappingInitMode = smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET;
		recognizerSettings.targets = new TargetEntry[1] { recognizerSettings.targets[0] };
		sceneMapFilePath_ = filePath;
		isLoadSceneMap_ = true;
		restartController();
	}

	void Awake () {
		DoCreate();
	}

	void Start () {
		GL.IssuePluginEvent(GetRenderEventFunc(), 0);
	}

	void OnEnable () {
	}

	void OnDisable () {
		DoDisable();
	}

	void OnDestroy () {
		DoDisable();
		DoDestroy();
	}

	void OnApplicationFocus (bool focus) {
	}

	void OnApplicationPause (bool pause) {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		DoDisable();
#endif
	}

	void OnApplicationQuit () {
		DoDisable();
		DoDestroy();
	}

	void Update () {
		if (smartInitFailed_) { return; }
		DoEnable();
		GetComponent<Camera>().fieldOfView = sarSmartar_SarSmartARController_sarGetFovy(self_);
	}

	void OnPreRender() {
		if (smartInitFailed_) { return; }
		drawStartTimeSec_ = Time.realtimeSinceStartup;

#if UNITY_5_0
#if UNITY_ANDROID && !UNITY_EDITOR
		GL.IssuePluginEvent(0);
#elif UNITY_IPHONE && !UNITY_EDITOR
		GL.Clear(true, false, Color.green);
#endif
#else
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
		GL.IssuePluginEvent(GetRenderEventFunc(), 0);
		GL.InvalidateState();
#else
		GL.Clear(true, true, Color.black);
#endif
#endif

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
#else
		sarSmartar_SarSmartARController_sarDoDraw(self_, Screen.width, Screen.height, miscSettings.showCameraPreview);
#endif

		// Draw landmarks
		if (miscSettings.showLandmarks) {
			// Get result
			var result = new smartar.RecognitionResult();
			result.maxLandmarks_ = smartar.Recognizer.MAX_NUM_LANDMARKS;
			result.landmarks_ = landmarkBuffer_;
			result.maxNodePoints_ = smartar.Recognizer.MAX_NUM_NODE_POINTS;
			result.nodePoints_ = nodePointBuffer_;
			result.maxInitPoints_ = smartar.Recognizer.MAX_NUM_INITIALIZATION_POINTS;
			result.initPoints_ = initPointBuffer_;
			GetResult(null, ref result);

			// Draw landmarks
			var adjustedPosition = new smartar.Vector3();
			var adjustedRotation = new smartar.Quaternion();
			sarSmartar_SarSmartARController_sarAdjustPose(self_, ref result.position_, ref result.rotation_, out adjustedPosition, out adjustedRotation);
			var mvMatrix = new smartar.Matrix44();
			smartar.Utility.convertPose2Matrix(adjustedPosition, adjustedRotation, out mvMatrix);
			var projMatrix = smartar.Utility.setPerspectiveM(GetComponent<Camera>().fieldOfView, (float) Screen.width / (float) Screen.height, 0.01f, 100.0f);
			var initPointMatrix = sarSmartar_SarSmartARController_sarGetInitPointMatrix(self_);

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
			// Draw Preview, landmarks, node points, init points
			sarSmartar_SarSmartARController_sarSetDrawData(Screen.width, Screen.height, miscSettings.showCameraPreview);
			landmarkDrawer_.SetData(projMatrix * mvMatrix, initPointMatrix, result);
			GL.IssuePluginEvent(GetRenderEventFunc(), (int)RenderEventID.DoDraw);
			GL.InvalidateState();
#else
			// Draw node points
			landmarkDrawer_.DrawNodePoints(projMatrix * mvMatrix, result.nodePoints_, result.numNodePoints_);

			// Draw landmarks
			landmarkDrawer_.DrawLandmarks(projMatrix * mvMatrix, result.landmarks_, result.numLandmarks_);

			// Draw init points
			landmarkDrawer_.DrawInitPoints(initPointMatrix, result.initPoints_, result.numInitPoints_);
#endif
		}
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
		else
		{
			sarSmartar_SarSmartARController_sarSetDrawData(Screen.width, Screen.height, miscSettings.showCameraPreview);
			GL.IssuePluginEvent(GetRenderEventFunc(), (int)RenderEventID.DoDraw);
			GL.InvalidateState();
		}
#else

		GL.InvalidateState();
#endif
	}

	void OnPostRender() {
		if (smartInitFailed_) { return; }

		sarSmartar_SarSmartARController_sarDoEndFrame(self_);

		drawTimeSec_ += Time.realtimeSinceStartup - drawStartTimeSec_;
	}

	void OnValidate() {
		bool isSceneMapping = recognizerSettings_.recognitionMode == smartar.RecognitionMode.RECOGNITION_MODE_SCENE_MAPPING;
		bool isInitModeTarget = recognizerSettings_.sceneMappingInitMode == smartar.SceneMappingInitMode.SCENE_MAPPING_INIT_MODE_TARGET;

		if (isSceneMapping && !isInitModeTarget && 0 < recognizerSettings_.targets.Length) {
			recognizerSettings_.targets = new TargetEntry[] { recognizerSettings_.targets[0] };
		}
	}

	public int GetResult(string id, ref smartar.RecognitionResult result)
	{
		int ret = -1;
		if (id == null || string.Equals(id, ""))
		{
			ret = sarSmartar_SarSmartARController_sarGetResult(self_, IntPtr.Zero, ref result);
			isRecognized_ = result.isRecognized_;
			return ret;
		}
		else
		{
			for (int i = 0, num = recognizerSettings.targets.Length; i < num; ++i)
			{
				if (string.Compare(recognizerSettings.targets[i].id, id) == 0)
				{
					if (targets_ == null) { break; }
					ret = sarSmartar_SarSmartARController_sarGetResult(self_, targets_[i].self_, ref result);
					isRecognized_ = result.isRecognized_;
					return ret;
				}
			}

			result = new smartar.RecognitionResult();
			isRecognized_ = result.isRecognized_;
			return smartar.Error.ERROR_INVALID_VALUE;
		}
	}

	public void adjustPose(smartar.Vector3 srcPosition, smartar.Quaternion srcRotation, out smartar.Vector3 rotPosition, out smartar.Quaternion rotRotation)
	{
		smartar.Rotation screenRotation_;
		screenDevice_.GetRotation(out screenRotation_);
		smartar.Rotation rotation = (smartar.Rotation)((cameraRotation_ - screenRotation_ + 360) % 360);

		rotPosition = srcPosition;
		rotRotation = srcRotation;
		if (isFlipX_) {
			rotPosition.x_ = -rotPosition.x_;

			rotRotation.y_ = -rotRotation.y_;
			rotRotation.z_ = -rotRotation.z_;
		}
		if (isFlipY_) {
			rotPosition.y_ = -rotPosition.y_;

			rotRotation.x_ = -rotRotation.x_;
			rotRotation.z_ = -rotRotation.z_;
		}
		if (rotation != smartar.Rotation.ROTATION_0) {
			float rad;
			switch (rotation) {
			case smartar.Rotation.ROTATION_90: {
					rad = Mathf.PI * 0.5f * 0.5f;
					break;
				}
			case smartar.Rotation.ROTATION_180: {
					rad = Mathf.PI * 1.0f * 0.5f;
					break;
				}
			case smartar.Rotation.ROTATION_270: {
					rad = Mathf.PI * 1.5f * 0.5f;
					break;
				}
			default:
				throw new InvalidOperationException("unexpected value: " + rotation);
			}
			// rotate pose around z axis
			Quaternion quat0 = new Quaternion(rotRotation.x_, rotRotation.z_, rotRotation.y_, rotRotation.w_);
			Quaternion quat1 = new Quaternion(0.0f, Mathf.Sin(rad), 0.0f, Mathf.Cos(rad));
			quat0 = quat0 * quat1;
			rotRotation.x_ = quat0.x;
			rotRotation.y_ = quat0.z;
			rotRotation.z_ = quat0.y;
			rotRotation.w_ = quat0.w;
		}
	}

	private struct CreateParam {
		public IntPtr smart_;
		public IntPtr sensorDevice_;
		public IntPtr screenDevice_;
		public IntPtr recognizer_;
		public IntPtr cameraImageDrawer_;
	}

	// get camera preview
	public void getImage(IntPtr image, out ulong timestamp) {
		sarSmartar_SarSmartARController_sarGetImage(self_, image, out timestamp);
	}

	public float getFovyFromSmartAR()
	{
		return sarSmartar_SarSmartARController_sarGetFovy(self_);
	}

	public void getCameraId(smartar.Facing facing, out int cameraId)
	{
		smartar.CameraDevice.GetDefaultCameraId(smart_, facing, out cameraId);
	}

#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern IntPtr sarSmartar_SarSmartARController_sarDoCreate(ref CreateParam param, bool workerThreadEnabled);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarDoDestroy(IntPtr self);
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarDoEnable(IntPtr self, IntPtr cameraDevice);
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarDoDisable(IntPtr self);
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarDoDraw(IntPtr self, int width, int height, bool showCameraImage);
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarDoEndFrame(IntPtr self);
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarGetResult(IntPtr self, IntPtr target, ref smartar.RecognitionResult result);
	[DllImport("__Internal")]
	private static extern float sarSmartar_SarSmartARController_sarGetFovy(IntPtr self);
	[DllImport("__Internal")]
	private static extern smartar.Matrix44 sarSmartar_SarSmartARController_sarGetInitPointMatrix(IntPtr self);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarAdjustPose(IntPtr self, ref smartar.Vector3 fromPosition, ref smartar.Quaternion fromRotation, out smartar.Vector3 toPosition, out smartar.Quaternion toRotation);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarRunWorkerThread(IntPtr self);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarFinishWorkerThread(IntPtr self);
	[DllImport("__Internal")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetCameraFrameCount(IntPtr self);
	[DllImport("__Internal")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetRecogCount(IntPtr self, int index);
	[DllImport("__Internal")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetRecogTime(IntPtr self, int index);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarSetTriangulateMasks(IntPtr self, smartar.Triangle2[] masks, int numMasks);
	[DllImport("__Internal")]
	private static extern void sarSmartar_SarSmartARController_sarGetImage(IntPtr self, IntPtr image, out ulong timestamp);
	[DllImport("__Internal")]
	private static extern IntPtr GetRenderEventFunc();
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern int sarSmartar_SarSmartARController_sarSetDrawData(int width, int height, bool showCameraImage);
#endif
#else
	[DllImport("smartar")]
	private static extern IntPtr sarSmartar_SarSmartARController_sarDoCreate(ref CreateParam param, bool workerThreadEnabled);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarDoDestroy(IntPtr self);
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarDoEnable(IntPtr self, IntPtr cameraDevice);
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarDoDisable(IntPtr self);
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarDoDraw(IntPtr self, int width, int height, bool showCameraImage);
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarDoEndFrame(IntPtr self);
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarGetResult(IntPtr self, IntPtr target, ref smartar.RecognitionResult result);
	[DllImport("smartar")]
	private static extern float sarSmartar_SarSmartARController_sarGetFovy(IntPtr self);
	[DllImport("smartar")]
	private static extern smartar.Matrix44 sarSmartar_SarSmartARController_sarGetInitPointMatrix(IntPtr self);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarAdjustPose(IntPtr self, ref smartar.Vector3 fromPosition, ref smartar.Quaternion fromRotation, out smartar.Vector3 toPosition, out smartar.Quaternion toRotation);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarRunWorkerThread(IntPtr self);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarFinishWorkerThread(IntPtr self);
	[DllImport("smartar")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetCameraFrameCount(IntPtr self);
	[DllImport("smartar")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetRecogCount(IntPtr self, int index);
	[DllImport("smartar")]
	private static extern ulong sarSmartar_SarSmartARController_sarGetRecogTime(IntPtr self, int index);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarSetTriangulateMasks(IntPtr self, smartar.Triangle2[] masks, int numMasks);
	[DllImport("smartar")]
	private static extern void sarSmartar_SarSmartARController_sarGetImage(IntPtr self, IntPtr image, out ulong timestamp);
	[DllImport("smartar")]
	private static extern IntPtr GetRenderEventFunc();

#if UNITY_STANDALONE_WIN || UNITY_EDITOR
	[DllImport("smartar")]
	private static extern int sarSmartar_SarSmartARController_sarSetDrawData(int width, int height, bool showCameraImage);
#endif
#endif
}
