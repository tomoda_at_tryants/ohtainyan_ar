﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	UISprite	m_Sprite;

	Color		m_Color;
	Color		m_FromColor;
	Color		m_ToColor;

	float		m_fFadeTime;
	float		m_fTime;

	bool		m_bFade;
	bool		m_bSustain;

	public bool isFade
	{
		get
		{
			return	m_bFade;
		}
	}

	// Use this for initialization
	void Start () {
		m_bFade		= false;
		m_Color		= Color.clear;

		try
		{
			m_Sprite = GetComponent<UISprite>();
			if (m_Sprite != null)	m_Sprite.enabled = false;
		}
		catch
		{
		}
	}
	
	// Update is called once per frame
	void Update () {

		float	t;
		if(m_bFade)
		{
			if(m_fFadeTime > 0)
			{
				m_fTime += Time.deltaTime;
				t = m_fTime / m_fFadeTime;
			}
			else
			{
				t = 1;
			}

			//	色補間処理
			m_Color			= Color.Lerp(m_FromColor, m_ToColor, t);
			m_Sprite.color	= m_Color;

			if(t >= 1)
			{
				m_bFade = false;
				m_fTime = 0f;
				m_Color = Color.clear;

				//	維持しない場合は非表示
				if (!m_bSustain)	m_Sprite.enabled = false;
			}
		}
	}

	/// <summary>
	/// フェードイン
	/// </summary>
	public void	In(float p_fFadeTime, Color p_FromColor)
	{
		m_bFade		= true;	//	フェード処理開始
		m_FromColor = p_FromColor;
		m_ToColor	= Color.clear;
		m_fFadeTime = p_fFadeTime;

		m_bSustain	= false;	//	維持しない

		//	表示
		m_Sprite.enabled = true;
	}

	/// <summary>
	/// フェードイン
	/// </summary>
	public void Out(float p_fFadeTime, Color p_ToColor, bool p_bSustain)
	{
		m_bFade		= true;		//	フェード処理開始
		m_FromColor = m_Sprite.color;
		m_ToColor	= p_ToColor;
		m_fFadeTime = p_fFadeTime;

		m_bSustain = p_bSustain;

		//	表示
		m_Sprite.enabled = true;
	}
}
