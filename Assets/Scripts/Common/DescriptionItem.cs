﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescriptionItem : MonoBehaviour
{
    /// <summary>
    /// テクスチャのパス
    /// </summary>
    const string    c_TexPath = "Textures/Products/";

    public UITexture                texture;
    public ConfigTable.Data     data;

    public static DescriptionItem   Create(ConfigTable.Data p_Data, GameObject p_Parent)
    {
            DescriptionItem me = ResourceManager.PrefabLoadAndInstantiate <DescriptionItem>("Common/DescriptionItem");
            GameUtils.AttachChild (p_Parent, me.gameObject);

            me.Init (p_Data);

            return me;
    }

    public void     Init(ConfigTable.Data p_Data)
    {
        if (p_Data == null)       return;

        data = p_Data;

        texture.mainTexture = ResourceManager.TextureLoad (c_TexPath + data.texName, "png");
        texture.MakePixelPerfect ();

        //  ８０％のサイズにする
        texture.width   = Mathf.FloorToInt(texture.width * 0.8f);
        texture.height  = Mathf.FloorToInt(texture.height * 0.8f);
    }
}
