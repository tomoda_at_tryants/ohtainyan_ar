﻿using UnityEngine;
using System.Collections;

public class ButtonTouchEffect : MonoBehaviour
{
	/// <summary>
	/// エフェクトが表示中かどうか
	/// </summary>
	static bool	c_bViewing;

	/// <summary>
	/// タッチされた時のエフェクトを表示する
	/// </summary>
	public void TouchEffect_Exec(UIButtonTween p_Sender)
	{
		StartCoroutine(TouchEffect_Exec0(p_Sender));
	}

	/// <summary>
	/// エフェクト表示処理
	/// </summary>
	/// <returns></returns>
	public IEnumerator	TouchEffect_Exec0(UIButtonTween p_Sender)
	{
		//	表示中のエフェクトが存在すればエフェクトは表示しない
		if (c_bViewing)	yield break;

		//	親設定
		Transform	a_Trans = p_Sender.transform;
		GameObject	a_Obj	= ResourceManager.PrefabLoadAndInstantiate("Common/TouchEffect", a_Trans.localPosition, a_Trans.parent);

		c_bViewing = true;

		yield return new WaitForSeconds(0.5f);

		UISprite	a_Background	= a_Obj.transform.FindChild("Background").GetComponent<UISprite>();
		Color		a_Color			= a_Background.color;

#if false
        //	０．５秒かけて透明にする
        yield return StartCoroutine(GameUtils.AlphaAnim(a_Color, 0.5f, 1, 0, (p_Color) =>
			{
				a_Background.color = p_Color;
			}
		));
#endif

		Destroy(a_Obj);

		c_bViewing = false;
	}
}
	