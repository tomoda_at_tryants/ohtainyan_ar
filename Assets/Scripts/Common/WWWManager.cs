﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class WWWManager : MonoBehaviour {

    static string   eTAG = "WWWManager";

    public enum eType
    {
        None = -1,

        Byte,
        Text
    }

    /// <summary>
    /// ダウンロード数
    /// </summary>
    [SerializeField]
    public int   downloadNum;

    DownloadProgress    m_DownloadProgress;
    public DownloadProgress downloadProgress
    {
        get
        {
            return  m_DownloadProgress;
        }
    }

    static WWWManager   c_Instance;
    public static WWWManager instance
    {
        get
        {
            return c_Instance;
        }

        set
        {
            c_Instance = value;
        }
    }
   
    /// <summary>
    /// ダウンロードが完了したか
    /// </summary>
    public bool isDone
    {
        get
        {
			Debug.Log ("ダウンロード数：" + downloadNum);
            return  (downloadNum > 0 || m_DownloadProgress != null) ? false : true;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static WWWManager    Create()
    {
        if (c_Instance != null) return  c_Instance;
                
        GameObject a_Obj    = new GameObject("WWWManager");
        c_Instance       = a_Obj.AddComponent<WWWManager>();

        DontDestroyOnLoad(a_Obj);

        return  c_Instance;
    }

    /// <summary>
    /// ダウンロード
    /// </summary>
    /// <param name="p_URL"></param>
    /// <param name="p_FileName"></param>
    /// <param name="p_SavePath"></param>
    /// <param name="p_Type"></param>
    public void Download(string p_URL, string p_FileName, string p_SavePath, eType p_Type, string p_Extension = "", System.Action<WWW> p_Callback = null)
    {
        StartCoroutine(Download_Exec(p_URL, p_FileName, p_SavePath, p_Type, p_Callback, p_Extension));
    }

    IEnumerator Download_Exec(string p_URL, string p_FileName, string p_SavePath, eType p_Type, System.Action<WWW> p_Callback, string p_Extension = "")
    {
        //  ダウンロード開始
        if (m_DownloadProgress == null && downloadNum == 0)
        {
            //  ダイアログ生成
            m_DownloadProgress = ResourceManager.PrefabLoadAndInstantiate<DownloadProgress>("Common/DownloadProgress");
            GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, m_DownloadProgress.gameObject);

            //  タッチを制御
            TouchDisable.instance.SetDisable(true);
        }

        //  ダウンロード数加算
        downloadNum++;

        //  ダウンロード開始
        WWW www = new WWW(p_URL);

        //  ダウンロードが完了するまで待機
        yield return www;

        //  エラー無し
        if (!ErrorCheck(www) && !string.IsNullOrEmpty(p_SavePath))
        {
            string  a_SavePath  = p_SavePath + p_FileName;
            string  a_Extension = "";

            switch (p_Type)
            {
                case eType.Byte:
                    {
                        a_Extension = (!string.IsNullOrEmpty(p_Extension)) ? p_Extension : ".png";

                        //  書き出し
                        File.WriteAllBytes(a_SavePath + a_Extension, www.bytes);
                    }
                    break;

                case eType.Text:
                    {
                        a_Extension = (!string.IsNullOrEmpty(p_Extension)) ? p_Extension : ".txt";

                        //  書き出し
                        File.WriteAllText(a_SavePath + a_Extension, www.text);
                    }
                    break;
            }

            //  ファイルの書き出しが完了するまで待機
            while (!File.Exists(a_SavePath + a_Extension))
            {
                yield return 0;
            }
        }

        //  ダウンロード終了時に呼ばれる
        Download_End(() =>
            {
                if (p_Callback != null) p_Callback(www);
            }
        );
    }

    public void Download_End(System.Action p_Callback)
    {
        downloadNum--;

        // 全てのダウンロードが完了した
        if (downloadNum <= 0)
        {
            downloadNum = 0;
            //  ダイアログ削除
            Destroy(m_DownloadProgress.gameObject);

            //  タッチ制御を解除
            TouchDisable.instance.SetDisable(false);
        }

        if (p_Callback != null) p_Callback();
    }

    /// <summary>
    /// エラーチェック
    /// </summary>
    /// <param name="p_WWW"></param>
    /// <returns></returns>
    public bool ErrorCheck(WWW p_WWW)
    {
        return (!string.IsNullOrEmpty(p_WWW.error) && (p_WWW.error != "Null" && p_WWW.error != "null")) ? true : false;
    }
}
