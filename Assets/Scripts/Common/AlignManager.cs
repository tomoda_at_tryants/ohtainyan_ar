﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// AlignTop, AlignMid, AlignBottomのアライメントを自動調整する
/// </summary>
//[ExecuteInEditMode]
public class AlignManager : MonoBehaviour {

	public enum eAlign
	{
		Top,
		Center,
		Bottom
	}

	public UIWidget		alignTop
	{
		get { return m_Aligns[eAlign.Top]; }
	}
	public UIWidget		alignCenter
	{
		get { return m_Aligns[eAlign.Center]; }
	}
	public UIWidget		alignBottom
	{
		get { return m_Aligns[eAlign.Bottom]; }
	}
	public UIWidget		align(eAlign p_Align)
	{
		return m_Aligns[p_Align];
	}



	public Dictionary<eAlign, UIWidget> aligns
	{
		get { return m_Aligns; }
	}
	Dictionary<eAlign, UIWidget>	m_Aligns;

	Vector2		m_LastOffset;

	// Use this for initialization
	void Start () {
		m_Aligns = new Dictionary<eAlign,UIWidget>();	

		UIWidget a_WD;
		a_WD = GameUtils.FindComponentByObjectName<UIWidget>(gameObject, "AlignTop");
		m_Aligns.Add(eAlign.Top, a_WD);

		a_WD = GameUtils.FindComponentByObjectName<UIWidget>(gameObject, "AlignCenter");
		m_Aligns.Add(eAlign.Center, a_WD);

		a_WD = GameUtils.FindComponentByObjectName<UIWidget>(gameObject, "AlignBottom");
		m_Aligns.Add(eAlign.Bottom, a_WD);

		m_LastOffset = Vector2.zero;

		Adjust();
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.isEditor)
		{
			if (m_Aligns == null)
			{
				Start();
			}
			Adjust();
		}
	}


	void Adjust()
	{
		AutoScreenSize a_AS = GetComponentInParent<AutoScreenSize>();
		if (a_AS == null) return;

		// 前回とオフセットが変化していなかったら何もしない
		if ((m_LastOffset.x == a_AS.offsetX) && (m_LastOffset.y == a_AS.offsetY))
		{
			return;
		}

		m_LastOffset.x = a_AS.offsetX;
		m_LastOffset.y = a_AS.offsetY;

		m_Aligns[eAlign.Top].bottomAnchor.absolute = (int)(-a_AS.offsetY);
		m_Aligns[eAlign.Top].topAnchor.absolute = (int)(-a_AS.offsetY + 2);

		m_Aligns[eAlign.Bottom].bottomAnchor.absolute = (int)(a_AS.offsetY);
		m_Aligns[eAlign.Bottom].topAnchor.absolute = (int)(a_AS.offsetY + 2);

	}
}
