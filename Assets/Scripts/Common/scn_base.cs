﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class scn_base : MonoBehaviour {

    const string eTAG = "scn_base";

    /// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
    {
        None,

        State_Init,
        State_Main,
        State_End,

        Max
    }

    //  ステート関連の処理
    #region

    /// <summary>
    ///	ステート名
    /// </summary>
    static Dictionary<eState, string>   c_StateList;

    eState  m_State;
    eState  m_NextState;
    string  m_Coroutine;

    /// <summary>
    /// ステート関連の初期化
    /// </summary>
    void State_Initialize()
    {
        //	ステートのリストを追加
        c_StateList = new Dictionary<eState, string>();
        for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
        {
            eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
            c_StateList.Add(a_State, a_State.ToString());
        }

        m_State     = eState.None;
        m_NextState = eState.State_Init;
    }

    /// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
    {
        m_NextState = p_NextState;
        if (p_bContinus)
        {
            State_Check();
        }
    }

    /// <summary>
    /// ステートに変化が無いかチェックし、変化があったらステートを切り替える
    /// </summary>
    /// <returns></returns>
    bool State_Check()
    {
        if (m_NextState != m_State)
        {
            m_State = m_NextState;
            if (!string.IsNullOrEmpty(m_Coroutine))
            {
                StopCoroutine(m_Coroutine);
                m_Coroutine = null;
            }

            m_Coroutine = c_StateList[m_State];
            StartCoroutine(m_Coroutine);

            TADebug.Log(eTAG, "Call " + m_Coroutine + "()");

            return true;
        }

        return false;
    }

    #endregion

    /// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs =
    {
//		new PrefInstantiater.Layout("Dummy",            "Dummy/Dummy",          false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero)),
		new PrefInstantiater.Layout("Dummy",            "Dummy/Dummy",          false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero)
	};
    PrefInstantiater    m_Instantiator;

    // Use this for initialization
    void Start()
    {
        //  ステート関連の初期化
        State_Initialize();
        //	「State_Init」を呼び出す
        State_Next(m_NextState, true);
    }

    IEnumerator State_Init()
    {
        //	共通の初期化
        bool    a_bWait = true;
        def.CommonInit(() =>
            {
                a_bWait = false;
            }
        );
        //	初期化が終了するまで待機
        while (a_bWait) yield return 0;

        //	使用するプレハブをインスタンス化
        m_Instantiator = PrefInstantiater.Create();
        m_Instantiator.Build(c_Prefs);

        State_Next(eState.State_Main, true);
    }

    IEnumerator State_Main()
    {
        while(true)
        {
            yield return 0;
        }

        State_Next(eState.State_End, true);
    }

    IEnumerator State_End()
    {
        yield return 0;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
