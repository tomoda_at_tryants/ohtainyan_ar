﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PanelManager : MonoBehaviour {

	static string eTAG = "PanelManager";

	/// <summary>
	/// レイヤー定義
	/// </summary>
	public enum eLayer
	{
		Low,
		Mid,
		High,
		Mask,
		Script,
		Touch,
	}

	/// <summary>
	/// シーン遷移時に破棄しない 
	/// </summary>
	public bool	dontDestroy;

	/// <summary>
	/// UIRootを取得する
	/// </summary>
	public UIRoot	uiRoot
	{
		get { return m_UIRoot; }
	}
	UIRoot	m_UIRoot;


	/// <summary>
	/// LowレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanLow
	{
		get { return m_Aligns[eLayer.Low]; }
	}

	/// <summary>
	/// MidレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanMid
	{
		get { return m_Aligns[eLayer.Mid]; }
	}
	public AlignManager	alignman(eLayer p_Layer)
	{
		return m_Aligns[p_Layer];
	}

	/// <summary>
	/// HighレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanHigh
	{
		get { return m_Aligns[eLayer.High]; }
	}

	/// <summary>
	/// MaskレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanMask
	{
		get { return m_Aligns[eLayer.Mask]; }
	}

	/// <summary>
	/// ScriptレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanScript
	{
		get { return m_Aligns[eLayer.Script]; }
	}

	/// <summary>
	/// TouchレベルのパネルのAlignManagerを取得する
	/// </summary>
	public AlignManager	alignmanTouch
	{
		get { return m_Aligns[eLayer.Touch]; }
	}

	public Dictionary<eLayer, AlignManager>	m_Aligns;


	/// <summary>
	/// LowレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelLow
	{
		get { return m_Panels[eLayer.Low]; }
	}
	/// <summary>
	/// MidレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelMid
	{
		get { return m_Panels[eLayer.Mid]; }
	}
	/// <summary>
	/// HighレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelHigh
	{
		get { return m_Panels[eLayer.High]; }
	}
	/// <summary>
	/// MaskレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelMask
	{
		get { return m_Panels[eLayer.Mask]; }
	}
	/// <summary>
	/// ScriptレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelScript
	{
		get { return m_Panels[eLayer.Script]; }
	}
	/// <summary>
	/// TouchレベルのUIPanelを取得する
	/// </summary>
	public UIPanel panelTouch
	{
		get { return m_Panels[eLayer.Touch]; }
	}

	/// <summary>
	/// ＵＩスクリーンの高さを取得する
	/// （端末画面の縦解像度ではない）
	/// </summary>
	public int	height
	{
		get { return m_UIRoot.manualHeight; }
	}

	/// <summary>
	/// ＵＩスクリーンの幅を取得する
	/// （端末画面の横解像度ではない）
	/// </summary>
	public int	width
	{
		get { return m_UIRoot.manualWidth; }
	}

	/// <summary>
	/// 任意レイヤーのUIPanelを取得する
	/// </summary>
	/// <param name="p_Layer"></param>
	/// <returns></returns>
	public UIPanel panel(eLayer p_Layer)
	{
		return m_Panels[p_Layer];
	}

	public Fade	fade;

	/// <summary>
	/// パネルハッシュテーブルを取得する
	/// ex) UIPanel a_Panel = panels[PanelManager.eLayer.Mid];
	/// </summary>
	public Dictionary<eLayer, UIPanel> panels
	{
		get { return m_Panels; }
	}
	Dictionary<eLayer, UIPanel>	m_Panels;

	static bool	c_bReady;
	/// <summary>
	/// 準備が出来たか
	/// </summary>
	public static bool	isReady
	{
		get { return c_bReady; }
	}

	static PanelManager s_Instance;

	/// <summary>
	/// インスタンス
	/// </summary>
	public static PanelManager instance
	{
		get { return s_Instance; }
	}

	public static void ColdInit()
	{
		s_Instance = null;
	}

	void Awake()
	{
		if (s_Instance != null)
		{
			Destroy(this);
			return;
		}

		s_Instance = this;
	}


	// Use this for initialization
	void Start ()
    {
		//	削除しない
		dontDestroy = true;

		if (dontDestroy)
		{
			// 全GameObjectにDontdestroyを設定する
			DontdestroySet(this.transform);
		}

		m_UIRoot = GetComponent<UIRoot>();

		UIPanel a_Panel;

		m_Panels = new Dictionary<eLayer,UIPanel>();

		// PanelLow
		a_Panel = GameObject.Find("PanelLow").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.Low, a_Panel);
		
		// PanelMid
		a_Panel = GameObject.Find("PanelMid").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.Mid, a_Panel);
		
		// PanelHigh
		a_Panel = GameObject.Find("PanelHigh").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.High, a_Panel);

		// PanelMask
		a_Panel = GameObject.Find("PanelMask").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.Mask, a_Panel);

		// PanelScript
		a_Panel = GameObject.Find("PanelScript").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.Script, a_Panel);

		// PanelTouch
		a_Panel = GameObject.Find("PanelTouch").GetComponent<UIPanel>();
		m_Panels.Add(eLayer.Touch, a_Panel);

		// AlignManager
		m_Aligns = new Dictionary<eLayer,AlignManager>();
		m_Aligns.Add(eLayer.Low,  m_Panels[eLayer.Low].GetComponent<AlignManager>());
		m_Aligns.Add(eLayer.Mid,  m_Panels[eLayer.Mid].GetComponent<AlignManager>());
		m_Aligns.Add(eLayer.High, m_Panels[eLayer.High].GetComponent<AlignManager>());
		m_Aligns.Add(eLayer.Mask, m_Panels[eLayer.Mask].GetComponent<AlignManager>());
		m_Aligns.Add(eLayer.Script, m_Panels[eLayer.Script].GetComponent<AlignManager>());
		m_Aligns.Add(eLayer.Touch, m_Panels[eLayer.Touch].GetComponent<AlignManager>());

		c_bReady = true;
	}
	
	/// <summary>
	/// 全GameObjectにDontdestroyを設定する
	/// </summary>
	void DontdestroySet(Transform p_Parent)
	{
		DontDestroyOnLoad(p_Parent.gameObject);
		
/*		for (int i = 0; i < p_Parent.childCount; i++)
		{
			DontdestroySet(p_Parent.GetChild(i));
		}
*/
	}

	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// 配下のオブジェクトを全てクリアする
	/// （PanelMaskは除く）
	/// </summary>
	public void Clean()
	{
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Low].alignTop.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Low].alignCenter.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Low].alignBottom.gameObject);

		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mid].alignTop.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mid].alignCenter.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mid].alignBottom.gameObject);

		GameUtils.RemoveChildAll(m_Aligns[eLayer.High].alignTop.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.High].alignCenter.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.High].alignBottom.gameObject);

		GameUtils.RemoveChildAll(m_Aligns[eLayer.Script].alignTop.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Script].alignCenter.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Script].alignBottom.gameObject);

		// マスクレイヤーのオブジェクトは破棄しない
/*
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mask].alignTop.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mask].alignCenter.gameObject);
		GameUtils.RemoveChildAll(m_Aligns[eLayer.Mask].alignBottom.gameObject);
 */ 
	}

}
