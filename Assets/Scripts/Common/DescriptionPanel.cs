﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescriptionPanel : MonoBehaviour {

    public List<DescriptionItem>    itemList;
    public UIGrid                   grid;
    public UIPanel                  scrollPanel;

    public UIButtonTween            backButton;
    public UIButtonTween            nextButton;

	public System.Action backButtonPushedCallback;
    public System.Action nextButtonPushedCallback;
                
    /// <summary>
    /// 生成
    /// </summary>
    /// <param name="p_NextButtonPushedCallback">P next button pushed callback.</param>
	public static DescriptionPanel  Create(System.Action p_BackButtonPushedCallback, System.Action p_NextButtonPushedCallback)
    {
            GameObject			a_Obj   = ResourceManager.PrefabLoadAndInstantiate("Common/DescriptionPanel", Vector3.zero);
            DescriptionPanel    me      = a_Obj.GetComponent<DescriptionPanel> ();

            GameUtils.AttachChild (PanelManager.instance.alignmanHigh.alignTop.gameObject, a_Obj);

			me.Init (p_BackButtonPushedCallback, p_NextButtonPushedCallback);

            return me;
    }
                
    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="p_NextButtonPushedCallback">P next button pushed callback.</param>
	public void     Init(System.Action p_BackButtonPushedCallback, System.Action p_NextButtonPushedCallback)
    {
        CSTransform.SetPos_Y(transform, -568);

		backButtonPushedCallback += p_BackButtonPushedCallback;
        nextButtonPushedCallback += p_NextButtonPushedCallback;

        grid.maxPerLine = def.instance.configTable.dataCount;

        //      アイテムの生成
        foreach(ConfigTable.Data a_Data in def.instance.configTable.dataList)
        {
            if (a_Data == null || string.IsNullOrEmpty (a_Data.texName))    continue;

            DescriptionItem a_Item = DescriptionItem.Create (a_Data, grid.gameObject);
        }

        //  スクロールサイズの調整
        float a_fAspect_def = def.c_ManualScreenSize.y / def.c_ManualScreenSize.x;
        float a_fAspect_current = (float)Screen.height / (float)Screen.width;


        float a_fSub = a_fAspect_def - a_fAspect_current;
        if(a_fSub > 0)
        {
            float   a_fOffset_Y = scrollPanel.clipOffset.y / a_fAspect_def * a_fAspect_current;
            float   a_fSize_Y   = scrollPanel.height / a_fAspect_def * a_fAspect_current;
            scrollPanel.SetRect(scrollPanel.clipOffset.x, 0, scrollPanel.width, a_fSize_Y - 40);
            scrollPanel.clipOffset = new Vector2(scrollPanel.clipOffset.x, 0);
        }
    }

    public void    NextButtonDidPush(UIButtonTween p_Sender)
    {
		if(nextButtonPushedCallback != null)	nextButtonPushedCallback ();
    }

    public void    BackButtonDidPush(UIButtonTween p_Sender)
    {
		if(backButtonPushedCallback != null)	backButtonPushedCallback();
        //      自身を削除する
        Destroy (gameObject);
    }
}
