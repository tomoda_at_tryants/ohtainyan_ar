﻿using UnityEngine;
using System.Collections;

public class AnimationEvent : MonoBehaviour {

	/// <summary>
	/// ＳＥの再生
	/// </summary>
	/// <param name="p_No"></param>
	public void	SePlay(SoundHelper.eNo p_No)
	{
		SoundHelper.instance.SePlay(p_No);
	}

	/// <summary>
	/// 削除
	/// </summary>
	public void	Destroy()
	{
		Destroy(gameObject);
	}

	bool m_bAnimation;
	public bool isAnimation
	{
		get { return m_bAnimation; }
	}

	/// <summary>
	/// アニメーションが再生されたら呼ばれる
	/// </summary>
	public void	AnimationStart()
	{
		m_bAnimation = true;
	}

	/// <summary>
	/// 現在再生しているアニメーションが終了したら呼ばれる
	/// </summary>
	public void	AnimationEnd()
	{
		m_bAnimation = false;
	}
}
