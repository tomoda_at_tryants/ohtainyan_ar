﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RenderingEffect : MonoBehaviour {

    public int      currentType;

    public bool     exe;
    bool            m_bStart; 

    /// <summary>
    /// プロパティ
    /// </summary>
    public struct stProperty
    {
        public int          type;
        public Vector2      fallSpeed;
        public Vector2      rotateSpeed;
        public float        interval;
        public string       texName;
        public List<Color>  color;
        public Vector2      size;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_fInterval"></param>
        /// <param name="p_SpriteName"></param>
        /// <param name="p_Color"></param>
        /// <param name="p_Size"></param>
        public stProperty(int p_nType, Vector2 p_FallSpeed, Vector2 p_RotateSpeed, float p_fInterval, string p_TexName, List<Color> p_Color, Vector2 p_Size)
        {
            type        = p_nType;
            fallSpeed   = p_FallSpeed;
            rotateSpeed = p_RotateSpeed;
            interval    = p_fInterval;
            texName     = p_TexName;
            color       = p_Color;
            size        = p_Size;
        }
    }

    public List<stProperty>    propertyList;

    public stProperty   currentProperty
    {
        get
        {
            foreach(stProperty property in propertyList)
            {
                if(property.type == currentType)
                {
                    return property;
                }
            }

            return  new stProperty();
        }
    }

    /// <summary>
    /// エフェクト数
    /// </summary>
    public int effectMax
    {
        get
        {
            if (propertyList == null) return 0;
            return  propertyList.Count;
        }
    }

    /// <summary>
    /// 生成間隔
    /// </summary>
    public float    interval
    {
        get
        {
            return  currentProperty.interval;
        }
    }

    static RenderingEffect          c_Instance;
    /// <summary>
    /// シングルトンインスタンス
    /// </summary>
    public static RenderingEffect   instance
    {
        get
        {
            if (c_Instance == null)   Create();
            return c_Instance;
        }
        set
        {
            if (c_Instance == null)   Create();
            c_Instance = value;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static void  Create()
    {
        GameObject      a_Obj   = new GameObject("RenderingEffect");
        GameUtils.AttachChild(PanelManager.instance.alignmanLow.alignTop.gameObject, a_Obj);

        c_Instance  = a_Obj.AddComponent<RenderingEffect>();
        c_Instance.Init();
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        exe         = false;
        currentType = 0;

        propertyList = new List<stProperty>()
        {
            new stProperty(0, Vector2.zero, Vector2.zero, 0, "", new List<Color>(), Vector2.zero),

            new stProperty
            (
                1,
                new Vector2( -3,  -1),
                new Vector2(-30,  30),
                0.5f,
                "effect_0001", 
                new List<Color>()
                {
                    Color.white,
                    Color.red,
                    Color.yellow,
                    Color.green,
                    Color.blue,
                    Color.cyan
                }, 
                new Vector2(32, 16)
            ),

            new stProperty
            (
                2,
                new Vector2(   -2f,  -0.5f),
                new Vector2(   -5,       5),
                1.2f,
                "effect_0002",
                new List<Color>()
                {
                    Color.white
                },
                new Vector2(46, 46)
            )
        };

        Effect.list.Clear();
    }

    /// <summary>
    /// エフェクトクラス
    /// </summary>
    public class Effect : MonoBehaviour
    {
        public static List<Effect> list = new List<Effect>();

        public static int num
        {
            get
            {
                if (list == null) return 0;
                return list.Count;
            }
        }

        public UITexture    tex;
        public float        fallSpeed;
        public float        fRotateSpeed;

        /// <summary>
        /// 生成
        /// </summary>
        /// <returns></returns>
        public static Effect    Create(RenderingEffect p_Parent)
        {
            stProperty  a_Property = p_Parent.currentProperty;

            GameObject  a_Obj   = new GameObject();
            GameUtils.AttachChild(p_Parent.gameObject, a_Obj);

            Effect      me      = a_Obj.AddComponent<Effect>();

            me.Init(UnityEngine.Random.Range(a_Property.fallSpeed.x, a_Property.fallSpeed.y), UnityEngine.Random.Range(a_Property.rotateSpeed.x, a_Property.rotateSpeed.y));

            CSTransform.SetEuler_Y(a_Obj.transform, UnityEngine.Random.Range(-45, 45));
            CSTransform.SetEuler_Z(a_Obj.transform, UnityEngine.Random.Range(-45, 45));

            list.Add(me);

            //  スプライト選択
            me.tex              = a_Obj.AddComponent<UITexture>();

            string  a_Path = string.Format("Textures/effectItems/{0}", a_Property.texName);
            me.tex.mainTexture  = ResourceManager.TextureLoad(a_Path, false);

            //  サイズ選択
            GameUtils.WidgetSize_Set(a_Obj, a_Property.size);
            GameUtils.WidgetDepth_Set(a_Obj, 8);

            //  カラー選択
            List<Color> a_Color = a_Property.color;
            int a_nIndex        = UnityEngine.Random.Range(0, a_Color.Count);
            me.tex.color     = a_Color[a_nIndex];

            //  位置
            CSTransform.SetPos_X(me.tex.transform, UnityEngine.Random.Range(-300, 300));

            return  me;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="p_fFallSpeed"></param>
        /// <param name="p_fRotateSpeed"></param>
        public void Init(float p_fFallSpeed, float p_fRotateSpeed)
        {
            fallSpeed       = p_fFallSpeed;
            fRotateSpeed    = p_fRotateSpeed;
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        public void Update()
        {
            try
            {
                //  移動
                CSTransform.AddPos_Y(transform, fallSpeed);
                //  回転
                transform.Rotate(fRotateSpeed, 0, 0);

                //  画面外は削除
                if (transform.localPosition.y <= (-def.c_ManualScreenSize.y - 100))
                {
                    Destroy(gameObject);
                    list.Remove(this);
                }
            }
            catch(Exception ex)
            {
            }
        }

        /// <summary>
        /// 全エフェクトを削除
        /// </summary>
        public static void AllDestroy()
        {
            //  リストをコピーする
            List<Effect>    a_Copy = new List<Effect>();
            foreach(Effect a_Effect in list)
            {
                a_Copy.Add(a_Effect);
            }

            //  オブジェクトの削除
            foreach(Effect a_Effect in a_Copy)
            {
                foreach(Effect effect in list)
                {
                    if(a_Effect == effect)
                    {
                        list.Remove(effect);
                        Destroy(effect.gameObject);

                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 変更
    /// </summary>
    /// <param name="p_Type"></param>
    public void Change(int p_nType)
    {
        //  停止
        Stop();

        StartCoroutine(Update_Exec(p_nType));
    }

    public IEnumerator  Update_Exec(int p_nType)
    {
        if (m_bStart)   yield break;

        yield return 0;

        currentType = p_nType;
        if (currentType == 0)  yield break;

        m_bStart    = true;
        exe         = true;

        float   a_fTime = 0;
        while(exe)
        {
            a_fTime += Time.deltaTime;
            if (a_fTime >= interval)
            {
                Effect.Create(this);
                a_fTime = 0;
            }

            //  エフェクトの更新処理
            int a_nCount = Effect.num;
            for(int i = 0 ; i < a_nCount ; i++)
            {
                if (Effect.list.Count >= i) break;

                Effect  a_Effect = Effect.list[i];
                a_Effect.Update();
            }

            yield return 0;
        }
    }

    /// <summary>
    /// 停止する
    /// </summary>
    public void Stop()
    {
        exe         = false;
        currentType = 0;
        m_bStart    = false;

        //  エフェクトを全削除
        Effect.AllDestroy();
    }
}
