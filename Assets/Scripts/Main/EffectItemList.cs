﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EffectItemList : MonoBehaviour {

    public UISprite     background;
    public GameObject   root;

    public delegate void    OpenDelegate();

    /// <summary>
    /// 開く時に呼ばれるコールバック
    /// </summary>
    public OpenDelegate     openCallback;

    public delegate void CloseDelegate();

    /// <summary>
    /// 閉じる時に呼ばれるコールバック
    /// </summary>
    public CloseDelegate    closeCallback;

    public delegate void    selectDelegate(int p_nNum);

    /// <summary>
    /// エフェクトが選択された時に呼ばれる処理
    /// </summary>
    public selectDelegate   selectCallback;

    /// <summary>
    /// アイテム生成開始位置
    /// </summary>
    Vector2 m_ItemBeginPos;

    List<EffectItem>    m_ItemList;

    /// <summary>
    /// 背景サイズ
    /// </summary>
    public Vector2 bgSize
    {
        get
        {
            Vector2 a_Size = GameUtils.WidgetSize_Get(background.gameObject);
            return  a_Size;
        }
    }

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static EffectItemList    Create()
    {
        EffectItemList  me = ResourceManager.PrefabLoadAndInstantiate<EffectItemList>("Main/effectItemList");
        me.Init();

        return me;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignBottom.gameObject, gameObject);
        CSTransform.SetPos_Y(transform, -(bgSize.y / 2));

        m_ItemBeginPos = new Vector2(-150, 0);

        m_ItemList = new List<EffectItem>();

        //  アイテムの生成
        int a_nMax = RenderingEffect.instance.effectMax;
        for (int i = 0 ; i < a_nMax; i++)
        {
            int a_nNum = i;
            EffectItem item = EffectItem.Create(a_nNum, root);

            Vector2 a_Pos = m_ItemBeginPos;
            a_Pos.x += (EffectItem.s_fViewInterval * i);

            item.transform.localPosition = a_Pos;

            item.selectCallback += Select;

            //  リストに追加
            m_ItemList.Add(item);
        }

        Frame_Update((int)RenderingEffect.instance.currentType);
    }

    public void CloseButtonDidPush()
    {
        Close();
    }

    /// <summary>
    /// 開く
    /// </summary>
    public void Open()
    {
        TouchDisable.instance.SetDisable(true);

        if (openCallback != null)   openCallback();
        iTween.MoveTo(gameObject, iTween.Hash("y", 100, "islocal", true, "easetype", iTween.EaseType.linear, "time", 0.3f, "oncomplete", "Opened_Exec", "oncompletetarget", gameObject));
    }

    /// <summary>
    /// 開いた後の処理
    /// </summary>
    void    Opened_Exec()
    {
        TouchDisable.instance.SetDisable(false);
    }

    /// <summary>
    /// 閉じる
    /// </summary>
    public void Close()
    {
        TouchDisable.instance.SetDisable(true);

        iTween.MoveTo(gameObject, iTween.Hash("y", -100, "islocal", true, "easetype", iTween.EaseType.linear, "time", 0.3f, "oncomplete", "Closed_Exec", "onCompletetarget", gameObject));
    }

    /// <summary>
    /// 閉じた後の処理
    /// </summary>
    void    Closed_Exec()
    {
        TouchDisable.instance.SetDisable(false);
        if (closeCallback != null)  closeCallback();
    }

    /// <summary>
    /// アイテムが選択された時に呼ばれる処理
    /// </summary>
    /// <param name="p_nNum"></param>
    void Select(int p_nNum)
    {
        //  フレームの更新を行う
        Frame_Update(p_nNum);

        RenderingEffect.instance.Change(p_nNum);

        if (selectCallback != null) selectCallback(p_nNum);
    }

    /// <summary>
    /// フレームの更新を行う
    /// </summary>
    void    Frame_Update(int p_nActiveNum)
    {
        foreach (EffectItem item in m_ItemList)
        {
            bool a_bFrame = (item.num == p_nActiveNum) ? true : false;
            item.Frame_ActiveSet(a_bFrame);
        }
    }
}
