﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CaptureImageView : MonoBehaviour
{
    public UISprite background;
    public UITexture image;
    public UIButtonTween saveButton;
    public UIButtonTween closeButton;

    CaptureManager captureManager;

    bool m_bMoveCapture;

    /// <summary>
    /// パネルが閉じる時に呼ばれるコールバック
    /// </summary>
    public System.Action closedCallback;

    public static CaptureImageView Create(CaptureManager p_CaptureMan, System.Action p_ClosedCallback, Texture2D p_Texture)
    {
        CaptureImageView me = ResourceManager.PrefabLoadAndInstantiate<CaptureImageView>("Main/CaptureImageView");

        GameUtils.AttachChild(PanelManager.instance.alignmanHigh.alignCenter.gameObject, me.gameObject);
        me.Init(p_CaptureMan, p_ClosedCallback, p_Texture);

        return me;
    }

    public void Init(CaptureManager p_CaptureMan, System.Action p_ClosedCallback, Texture2D p_Texture)
    {
        BackgroundSetting();
        captureManager = p_CaptureMan;

        closedCallback = p_ClosedCallback;

        //  キャプチャ画像をセット
        image.mainTexture = p_Texture;
        image.MakePixelPerfect();

        Vector2 a_CaptureSize = Vector2.zero;
        //  横合わせ
        float   a_fManualWidth  = PanelManager.instance.uiRoot.manualWidth;
        float   a_fRate         = a_fManualWidth / image.mainTexture.width;

        a_CaptureSize   = new Vector2((float)(image.mainTexture.width) * a_fRate, image.mainTexture.height * a_fRate);
        int a_nWidth    = PanelManager.instance.uiRoot.manualWidth - Mathf.FloorToInt(a_CaptureSize.x);
        int a_nHeight   = PanelManager.instance.uiRoot.manualHeight - Mathf.FloorToInt(a_CaptureSize.y);

        if (a_nWidth > 0)   a_CaptureSize.x += a_nWidth;
        if (a_nHeight > 0)  a_CaptureSize.y += a_nHeight;

        GameUtils.WidgetSize_Set(image.gameObject, a_CaptureSize);

        UIAnchor[]  a_Anchors = transform.GetComponentsInChildren<UIAnchor>();
        foreach (UIAnchor a_Anchor in a_Anchors)
        {
            a_Anchor.enabled = true;
        }
    }

    //  「Background」に関する設定
    public void BackgroundSetting()
    {
        BoxCollider a_Collider = background.GetComponent<BoxCollider>();
        a_Collider.size = def.c_ScreenSize;

        GameUtils.WidgetSize_Set(background.gameObject, new Vector2(def.c_ScreenSize.x + 100, def.c_ScreenSize.y + 100));
    }

    public void SaveButtonDidPush(UIButtonTween p_Sender)
    {
        //  画像パス更新
        ImagePathUpdate();

        //  保存が完了したらボタンは非表示にする
        p_Sender.gameObject.SetActive(false);
    }

    public void ShareButtonDidPush(UIButtonTween p_Sender)
    {
        //  画像パス更新
        ImagePathUpdate();

        string  a_Value = p_Sender.generalData.stringValue;
        string  a_Text  = "太田胃にゃん　テスト中";

        //	指定したSNSに投稿する
        NativePlugin.SNSPostManager.eSNS    a_SNS = NativePlugin.SNSPostManager.getSNSType(a_Value);
        NativePlugin.SNSPostManager.Post(a_SNS, captureManager.imagePath, a_Text);

        //	クリップボードにコピー
        NativePlugin.ClipboardManager.Set(a_Text);
    }

    void ImagePathUpdate()
    {
        if (!m_bMoveCapture)
        {
            //  キャプチャ画像の存在するパスを更新
            captureManager.moveCaptureImage();
            m_bMoveCapture = true;
        }
    }

    public void CloseButtonDidPush(UIButtonTween p_Sender)
    {
        //  キャプチャ画像を削除する
        if (File.Exists(captureManager.imagePath) && !m_bMoveCapture)
        {
            File.Delete(captureManager.imagePath);
        }

        closedCallback();
        Destroy(gameObject);
    }
}
