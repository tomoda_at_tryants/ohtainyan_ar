﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;

public class CaptureManager : MonoBehaviour
{
    SmartARController smartARController;
    string m_ImageName;
    public string imageName { get { return m_ImageName; } }

    string m_ImagePath;
    public string imagePath
    {
        get
        {
            string a_ImagePath = "";

#if UNITY_IPHONE && !UNITY_EDITOR
			a_ImagePath = m_ImagePath;
#elif UNITY_ANDROID && !UNITY_EDITOR
            a_ImagePath = (m_bMovedImagePath) ? m_ScanImagePath : m_ImagePath;
#endif
            return a_ImagePath;
        }
    }

    /// <summary>
    /// 画像の保存先が変更後のパス
    /// </summary>
    string m_ScanImagePath;

    /// <summary>
    /// キャプチャ画像の保存先パスが変更された
    /// </summary>
    bool m_bMovedImagePath;

    public byte[] imageData;

    /// <summary>
    /// 撮影し、保存する
    /// </summary>
    public void Shot(System.Action p_Callback)
    {
        //  コンストラクタでエラーが発生すれば終了
//      if (smartARController.smart_.isConstructorFailed()) return;

        //  パスの設定
        SetPath();

        if (string.IsNullOrEmpty(m_ImageName)) return;

        //  撮影前に行う処理
        PreCaptureImage_Exec();
        //  スクリーンショット
        Application.CaptureScreenshot(m_ImageName);
        m_ImageName = "";
        imageData   = null;

        //  キャプチャがローカルに保存されたらコールバックが呼ばれる
        StartCoroutine(waitUntilFinishedWriting((p_ImageData) =>
            {
                //  撮影後に呼ばれる処理
                StartCoroutine(ShotFinished_Exec(() =>
                    {
                        if (p_Callback != null) p_Callback();
                    },
                    p_ImageData
                ));
            }
        ));
    }

    /// <summary>
    /// 撮影処理終了後に呼ばれる処理
    /// </summary>
    /// <returns>The finished exec.</returns>
    public IEnumerator ShotFinished_Exec(System.Action p_Callback, byte[] p_ImageData)
    {
        imageData = p_ImageData;

        Texture2D a_Tex = new Texture2D(0, 0);
        a_Tex.LoadImage(imageData);

        //  キャプチャ画像を表示するビュー
        CaptureImageView a_ImageMan = CaptureImageView.Create(this, p_Callback, a_Tex);

        yield return 0;
    }

    /// <summary>
    /// 保存先のパスの設定を行う
    /// </summary>
    private void SetPath()
    {
        m_ImageName = string.Format("capture_image_{0}.png", DateTime.Now.ToString("d-MM-yyyy-HH-mm-ss-f"));
        m_ImagePath = GetImagePath(m_ImageName);
    }

    /// <summary>
    /// キャプチャしたイメージの保存先パスを取得する
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string GetImagePath(string name)
    {
        string a_Path;
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
                {
                    a_Path = string.Format("{0}/{1}", Application.persistentDataPath, name);
                }
                break;

            case RuntimePlatform.WindowsPlayer:
                {
                    a_Path = string.Format("{0}/{1}", Application.dataPath, name);
                }
                break;

            default:
                {
                    a_Path = name;
                }
                break;
        }

        return a_Path;
    }

    /// <summary>
    /// 撮影前に呼ばれる処理
    /// </summary>
    void PreCaptureImage_Exec()
    {
        if (Application.isEditor) return;

        NativePlugin.ShutterManager.Play();
    }

    //  DllImport関連
#if UNITY_IPHONE && !UNITY_EDITOR
    /// <summary>
    /// シャッター音を再生する
    /// </summary>
    [DllImport("__Internal")]
    private static extern void _PlaySystemShutterSound();

    /// <summary>
    /// イメージを書き込む
    /// </summary>
    /// <param name="path"></param>
    [DllImport("__Internal")]
    private static extern void _WriteImageToAlbum(string path);
#endif

    /// <summary>
    /// 画像がローカルに保存されるまで待機する
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    private IEnumerator waitUntilFinishedWriting(System.Action<byte[]> callback)
    {
        while (!File.Exists(m_ImagePath))
        {
            yield return null;
        }

        //  画像の存在が確認できても読み込むには少しウェイトが必要なので
        yield return new WaitForSeconds(1);

        byte[] a_ImageData = File.ReadAllBytes(m_ImagePath);

        callback(a_ImageData);
    }

    /// <summary>
    /// キャプチャ画像を移動する（SNSにシェアする時にキャプチャの移動が必要）
    /// </summary>
    public void moveCaptureImage()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        _WriteImageToAlbum(m_ImagePath);
#elif UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaObject utils = new AndroidJavaObject("com.sony.smartar.utils.CaptureImageUtils"))
        {
            m_ScanImagePath = utils.CallStatic<string>("moveToExternalDir", m_ImagePath);
            if (string.IsNullOrEmpty(m_ScanImagePath))      return;
            utils.CallStatic("scanCaptureImage", m_ScanImagePath);
        }
#endif

        m_bMovedImagePath = true;
    }
}
