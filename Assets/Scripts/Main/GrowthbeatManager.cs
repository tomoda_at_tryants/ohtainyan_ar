﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthbeatManager : MonoBehaviour
{
        [SerializeField]
        private string  applicationID;

        [SerializeField]
        private string credentialID;

        [SerializeField]
        private string senderID;

        void Awake()
        {
                GrowthPush.GetInstance().Initialize(applicationID, credentialID, Debug.isDebugBuild ? GrowthPush.Environment.Development : GrowthPush.Environment.Production);
                GrowthPush.GetInstance ().RequestDeviceToken (senderID);

                //      タグ送信
                GrowthPush.GetInstance ().SetTag ("Development", "true");

                //      イベント送信
                GrowthPush.GetInstance().TrackEvent("Launch");
        }

        // iOS のデバイストークン取得
        // デバイストークンが NotificationServices から戻ってくるため Update にて SetDeviceToken を実装
        bool m_bTokenSent = false;

        void    Update()
        {
#if UNITY_IPHONE
                if(!m_bTokenSent)
                {
                        byte[] a_Token = UnityEngine.iOS.NotificationServices.deviceToken;
                        if(a_Token != null)
                        {
                                GrowthPush.GetInstance ().SetDeviceToken(System.BitConverter.ToString(a_Token).Replace("-", "").ToLower());
                                m_bTokenSent = true;
                        }
                }
#endif
        }
}
