﻿using UnityEngine;
using System.Collections;

public class EffectItem : MonoBehaviour {

    /// <summary>
    /// 番号
    /// </summary>
    public int              num;

    [SerializeField]
    UITexture               tex;

    [SerializeField]
    UITexture               frame;

    public UIButtonTween    button;

    /// <summary>
    /// 表示間隔
    /// </summary>
    public static float s_fViewInterval;

    public delegate void    selectDelegate(int p_nNum);

    /// <summary>
    /// エフェクトが選択された時に呼ばれる処理
    /// </summary>
    public selectDelegate   selectCallback;

    /// <summary>
    /// 生成
    /// </summary>
    /// <returns></returns>
    public static EffectItem    Create(int p_nNum, GameObject p_Parent)
    {
        EffectItem  me  = ResourceManager.PrefabLoadAndInstantiate<EffectItem>("Main/EffectItem");
        me.Init(p_nNum, p_Parent);

        return  me;
    }

    public void Init(int p_nNum, GameObject p_Parent)
    {
        num = p_nNum;

        string  a_TexName = string.Format("selectitem_{0}", p_nNum.ToString().PadLeft(4, '0'));

        tex.mainTexture = ResourceManager.TextureLoad("Textures/effectItems/" + a_TexName, false);

        GameUtils.AttachChild(p_Parent, gameObject);
        GameUtils.WidgetSize_Set(tex.gameObject, new Vector2(100, 150));

        if(s_fViewInterval == 0)
        {
            s_fViewInterval = 150;
        }
    }

    /// <summary>
    /// 選択された時の処理
    /// </summary>
    public void Select()
    {
        if (selectCallback != null) selectCallback(num);
    }

    /// <summary>
    /// フレームの表示を設定する
    /// </summary>
    public void Frame_ActiveSet(bool p_bActive)
    {
        frame.gameObject.SetActive(p_bActive);
    }
}
