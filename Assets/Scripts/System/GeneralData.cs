using UnityEngine;
using System.Collections;

public class GeneralData : MonoBehaviour {

	public int		intValue;
	public string	stringValue;
	public System.Object	objValue;
	
	public int[]	intArrayValue;
	public string[]	stringArrayValue;
	public System.Object[]	objArrayValue;
	
}
