﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundHelper : MonoBehaviour
{

	//
	// Change by app
	//
	public enum eNo : int
	{
		Dummy,
    };

	//
	// Change by app
	//
	static stSoundTable[] c_SoundTable = 
	{
		//                eNo					fileName				            track, priority, comment
		new stSoundTable( eNo.Dummy,			null, 					            0, 0,	"ダミー"		),

//	BGM
		new stSoundTable( eNo.Dummy,            null,                               0, 0,   "ダミー"       ),

//	JGL
		new stSoundTable( eNo.Dummy,            null,                               0, 0,   "ダミー"       ),
		
//	SE
        new stSoundTable( eNo.Dummy,            null,                               0, 0,   "ダミー"       ),
    };

	/// <summary>
	/// データテーブル
	/// </summary>
	struct stSoundTable
	{
		public eNo no;
		public string fileName;

		public int trackNo;
		public int priority;

		public string comment;

		public stSoundTable(eNo p_No, string p_FileName, int p_TrackNo, int p_Priority, string p_Comment)
		{
			no = p_No;
			fileName = p_FileName;
			trackNo = p_TrackNo;
			priority = p_Priority;
			comment = p_Comment;
		}
	};


	Dictionary<int, AudioClip> m_Clips = new Dictionary<int, AudioClip>();		// = new Dictionary<int, AudioClip>();

	static SoundHelper s_Instance;
	public static SoundHelper instance
	{
		get
		{
			if (s_Instance == null)
			{
				Create();
			}
			return s_Instance;
		}
	}

	public static SoundHelper Create()
	{
		if (s_Instance != null)
		{
			return s_Instance;
		}

		GameObject obj = new GameObject("SoundHelper");
		s_Instance = obj.AddComponent<SoundHelper>();

		return s_Instance;

	}

	// テストモード関連
	int m_nTestNo = 1;
	bool m_bTestMode = true;
	bool testmode
	{
		get { return m_bTestMode; }
		set { m_bTestMode = value; }
	}

	bool m_bInitialized;
	public bool isReady
	{
		get { return m_bInitialized; }
	}

	/// <summary>
	/// Awake
	/// </summary>
	void Awake()
	{
		if ((s_Instance != null) && (s_Instance != this))
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		m_bInitialized = false;

		//		TADebug.Create();
		//		TADebug.GUIDrawDelegateAppend("Sound", OnGUISoundTest);

		s_Instance = this;
	}

	/// <summary>
	/// Start
	/// </summary>
	/// <returns></returns>
	IEnumerator Start()
	{
		yield return 0;
		foreach (stSoundTable table in c_SoundTable)
		{
			if (table.fileName != null)
			{
				AudioClip clip = ResourceManager.AudioClipLoad(table.fileName, false);
				m_Clips.Add((int)table.no, clip);
//				Debug.Log("AudioClip Loaded = " + table.fileName);
			}
		}
		yield return 0;

		SoundManager.instance.clips = m_Clips;

		m_bInitialized = true;
	}

	// Update is called once per frame
	void Update()
	{

	}

	/// <summary>
	/// 全サウンド停止
	/// </summary>
	public void StopAll()
	{
		BgmStop();
		SeStop();
	}

	/// <summary>
	/// SE再生
	/// </summary>
	/// <param name="p_No"></param>
	public void SePlay(eNo p_No)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false);
	}

	public void	SePlay(eNo p_No, bool p_bLoop, float p_fVolume)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false, false, p_fVolume);
	}

	public void SePlay(eNo p_No, bool p_bLoop)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, SoundManager.c_DefaultPitch, false, p_bLoop);
	}

	public void SePlay(eNo p_No, float p_Pitch)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, p_Pitch, false);
	}

	public void SePlay(eNo p_No, float p_Pitch, bool p_bMultiplex)
	{
		SoundManager.instance.SePlay((int)p_No, c_SoundTable[(int)p_No].trackNo, c_SoundTable[(int)p_No].priority, p_Pitch, p_bMultiplex);
	}

	public void SeStop()
	{
		SoundManager.instance.SeStop();
	}

	public void BgmPlay(eNo p_No)
	{
		SoundManager.instance.BgmPlay((int)p_No, true);
	}

	public void BgmPlay(eNo p_No, bool p_Loop)
	{
		SoundManager.instance.BgmPlay((int)p_No, p_Loop);
	}

	public void BgmPlay(eNo p_No, bool p_Loop, float p_fVol)
	{
		SoundManager.instance.BgmPlay((int)p_No, p_Loop, p_fVol);
	}

	public void BgmStop(float p_FadeTime = 0)
	{
		SoundManager.instance.BgmStop(p_FadeTime);
	}

	public void	BgmVolumeSet(float p_fVol)
	{
		SoundManager.instance.BgmVolumeSet(p_fVol);
	}

	#region FOR DEBUG

	Rect m_TestWinRect = new Rect(0, 0, 480, 320);

	/// <summary>
	/// サウンドテストモード
	/// </summary>
	public void OnGUISoundTest(int p_nWindowID)
	{
		m_TestWinRect = GUI.Window(p_nWindowID, m_TestWinRect, drawGUI, "Sound Test");
	}

	public void drawGUI(int p_WindowID)
	{

		GUILayout.BeginVertical("box");

		GUILayout.BeginHorizontal("box");

		if (GUILayout.Button(" < "))
		{
			m_nTestNo--;
			if (m_nTestNo <= 0)
			{
				m_nTestNo = c_SoundTable.Length - 1;
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button("PLAY"))
		{
			if (c_SoundTable[m_nTestNo].trackNo == 0)
			{
				BgmPlay((eNo)m_nTestNo);
			}
			else
			{
				SePlay((eNo)m_nTestNo);
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button("STOP"))
		{
			if (c_SoundTable[m_nTestNo].trackNo == 0)
			{
				BgmStop();
			}
			else
			{
				SeStop();
			}
		}

		GUILayout.Space(20);

		if (GUILayout.Button(" > "))
		{
			m_nTestNo++;
			if (m_nTestNo >= c_SoundTable.Length)
			{
				m_nTestNo = 1;
			}
		}

		GUILayout.EndHorizontal();


		GUILayout.BeginHorizontal("box");
		GUILayout.Label(c_SoundTable[m_nTestNo].fileName);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal("box");
		GUILayout.Label(c_SoundTable[m_nTestNo].comment);
		GUILayout.EndHorizontal();

		GUILayout.EndVertical();

		GUI.DragWindow(new Rect(0, 0, 300, 150));
	}

	#endregion

}
