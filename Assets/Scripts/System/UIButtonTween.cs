﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(UIButton))]
[RequireComponent(typeof(GeneralData))]
public class UIButtonTween : MonoBehaviour
{

	static string eTAG = "UIButtonTween";

	// ディレイ（タッチからonClickが呼ばれるまでの時間）
	static float c_OnClickDelay = 0.3f;

	// アニメーションデータ
	static string c_ButtonAnimatorFile = "ButtonCtrl";

	static RuntimeAnimatorController s_AnimatorController;

	static UICamera.MouseOrTouch s_CurrentTouch;
	public static UICamera.MouseOrTouch currentTouch
	{
		get { return s_CurrentTouch; }
	}

	static Collider s_CurrentCollider;
	public static Collider currentCollider
	{
		get { return s_CurrentCollider; }
	}

	static float s_nBusy;

	// プロパティー

	public bool enableIdle = false;

	/// <summary>
	/// ディレイ（タッチからonClickが呼ばれるまでの時間。-1でデフォルト値）
	/// </summary>
	public float delay = -1;
	public int animationType = 0;
	public int depthOffset = 0;
	public bool spChangeAtDisable = false;

	/// <summary>
	/// クリックの判定をボタンのリリース時に行う
	/// </summary>
	public bool clickAtRelease = false;

	/// <summary>
	/// 長押し判定を行う
	/// ※clickAtReleaseはtrueに設定すること
	/// </summary>
	public bool longPress = false;

	/// <summary>
	/// クリック時のコール先を設定する
	/// </summary>
	public EventDelegate onClick;

	/// <summary>
	/// 長押し時のコール先を設定する
	/// </summary>
	public EventDelegate onLongPress;

	/// <summary>
	/// デフォルトの効果音か
	/// </summary>
	public bool m_bSeDefault = true;

	/// <summary>
	/// ＳＥ（デフォルトで無い場合のみ設定）
	/// </summary>
	public SoundHelper.eNo	m_SeNo = SoundHelper.eNo.Dummy;

	/// <summary>
	/// クリックイベントを設定する
	/// イベントはUIButtonTweenの引数を受け取るもの
	/// </summary>
	/// <param name="p_Sedner"></param>
	/// <param name="p_Method"></param>
	/// <param name="p_StringKey"></param>
	/// <param name="p_nParam"></param>
	public void onClickEventSet(MonoBehaviour p_Sedner, string p_Method, string p_StringKey, int p_nParam)
	{
		EventDelegate a_OnClickEvent = new EventDelegate(p_Sedner, p_Method);
		EventDelegate.Parameter a_Param = a_OnClickEvent.parameters[0];
		a_Param.obj = this;

		this.stringValue = p_StringKey;
		this.intValue = p_nParam;

		onClick = a_OnClickEvent;
	}
	public void onClickEventSet(MonoBehaviour p_Sedner, string p_Method, string p_StringKey, System.Object p_Param)
	{
		EventDelegate a_OnClickEvent = new EventDelegate(p_Sedner, p_Method);
		EventDelegate.Parameter a_Param = a_OnClickEvent.parameters[0];
		a_Param.obj = this;

		this.stringValue = p_StringKey;
		this.objValue = p_Param;

		onClick = a_OnClickEvent;
	}
	public void onLongPressEventSet(MonoBehaviour p_Sedner, string p_Method, string p_StringKey, System.Object p_Param)
	{
		EventDelegate a_OnLongPressEvent = new EventDelegate(p_Sedner, p_Method);
		EventDelegate.Parameter a_Param = a_OnLongPressEvent.parameters[0];
		a_Param.obj = this;

		this.stringValue = p_StringKey;
		this.objValue = p_Param;

		onLongPress = a_OnLongPressEvent;
	}

	/// <summary>
	/// クリック時のサブイベントを設定する
	/// チュートリアルなどでボタンの入力で処理を進めたいときなどに設定。（メインの処理を変えたいときは使わない）
	/// イベントはUIButtonTweenの引数を受け取るもの
	/// </summary>
	/// <param name="p_Sedner"></param>
	/// <param name="p_Method"></param>
	/// <param name="p_StringKey"></param>
	/// <param name="p_nParam"></param>
	public void onClickSubEventSet(System.Action p_OnClicked)
	{
		m_OnSubClicked = p_OnClicked;
	}

	System.Action m_OnSubClicked;

	RuntimeAnimatorController m_AnimatorController;
	Animator m_Animator;
	UIButton m_Button;
	BoxCollider m_Collider;

	UICamera.MouseOrTouch m_CurrentTouch;
	Collider m_CurentCollider;

	GeneralData m_GeneralData;

	public GeneralData generalData
	{
		get { return m_GeneralData; }
	}

	/// <summary>
	/// 文字列値を取得する
	/// </summary>
	public string stringValue
	{
		get { return ((m_GeneralData != null) ? m_GeneralData.stringValue : null); }
		set 
		{ 
			if(m_GeneralData == null)
			{
				m_GeneralData = gameObject.GetComponent<GeneralData>();
			}
			m_GeneralData.stringValue = value; 
		}
	}

	/// <summary>
	/// 整数値を取得する
	/// </summary>
	public int intValue
	{
		get { return m_GeneralData.intValue; }
		set { m_GeneralData.intValue = value; }
	}

	/// <summary>
	/// オブジェクトを取得する
	/// </summary>
	public System.Object objValue
	{
		get { return m_GeneralData.objValue; }
		set { m_GeneralData.objValue = value; }
	}

	/// <summary>
	/// UIButtonのインスタンスを取得する
	/// </summary>
	public UIButton uiButton
	{
		get { return m_Button; }
	}

	/// <summary>
	/// ドラッグが終了したら呼ばれるコールバック
	/// </summary>
	public System.Action<Vector2> onDragEnd;

	// Release時にクリックする場合、DragOutとDragEndが同じタイミングで呼ばれていたら、DragOutを無視する
	float m_DragOutTime;


	static string[] c_SpName =
	{
		"p_bt_0_g",	"p_bt_0"
	};

	bool m_bStarted = false;
	bool m_bAnimating = false;

	// ドラッグしてしまった時に判定がスカるのを防ぐ為の処理用
	bool m_bPressed = false;
	Vector2 m_DragMove;

	// 長押し処理しているか？
	bool m_bLongPressed;

	// 入力を許可するボタンのリスト（空なら全ての入力を受け付ける）
	static List<UIButtonTween> s_MaskTargets = new List<UIButtonTween>();

	public static void ColdInit()
	{
		s_AnimatorController = null;
		s_CurrentCollider = null;
		s_CurrentTouch = null;
		s_nBusy = 0;
		s_MaskTargets = new List<UIButtonTween>();
	}

	void Awake()
	{
		if (s_AnimatorController == null)
		{
			//			s_AnimatorController = ResourceManager.AnimatorLoad("Animation/ButtonAnimatorCtrl");
			s_AnimatorController = (RuntimeAnimatorController)Resources.Load(def.c_AnimPath + c_ButtonAnimatorFile, typeof(RuntimeAnimatorController));
		}

		Init();

	}

	void Start()
	{
	}

	void Init()
	{
		if (!m_bStarted)
		{
			m_Animator = GetComponent<Animator>();
			m_AnimatorController = m_Animator.runtimeAnimatorController;
			if (m_AnimatorController == null)
			{
				m_AnimatorController = (RuntimeAnimatorController)Instantiate(s_AnimatorController);
			}

			// アニメーション
			m_Animator.runtimeAnimatorController = m_AnimatorController;

			// ボタン
			m_Button = gameObject.GetComponent<UIButton>();

			// アニメーションイベントを追加する
/*			TAAnimatorEvent ev = gameObject.GetComponent<TAAnimatorEvent>();
			if (ev == null)
			{
				ev = gameObject.AddComponent<TAAnimatorEvent>();
			}
*/
			// コライダー
			m_Collider = GetComponent<BoxCollider>();
			if (m_Collider == null)
			{
				NGUITools.AddWidgetCollider(gameObject);
				m_Collider = GetComponent<BoxCollider>();
			}

			if (!enableIdle)
			{
				m_Animator.enabled = false;
			}

			// GeneralData
			m_GeneralData = gameObject.GetComponent<GeneralData>();

			m_bStarted = true;

			OnEnable();
		}

		if (delay == -1)
		{
			delay = c_OnClickDelay;
		}
	}

	void Update()
	{
		if (!m_bStarted) return;
	}

	void OnEnable()
	{
		if (m_bStarted)
		{
			if (enableIdle)
			{
				m_Animator.enabled = true;
				idleEnable(true);
			}
			else
			{
				m_Animator.enabled = false;
				idleEnable(false);
			}
			m_Button.enabled = true;
		}
	}

	void OnDisable()
	{
		if (m_bStarted)
		{
			m_Animator.Play("Default");
			m_Button.enabled = false;
		}
	}

	/// <summary>
	/// クリック時に呼ぶ
	/// （OnPress()から呼ばれる）
	/// </summary>
	protected virtual void _onClick()
	{
		//		TADebug.Log(eTAG, "_onClick " + gameObject.name);

		if (s_nBusy > 0)
		{
			return;
		}

		ForceReleaseOtherButton();
		setBusyTimer(delay + 0.1f);	// アニメーションdelayの後も少しbusyの状態を続ける→アニメーション完了直後にボタンが押下出来てしまうタイミングが発生するため

		//		m_CurrentTouch = UICamera.currentTouch;
		StartCoroutine("_onClickDelayed", delay);
	}

	IEnumerator _onClickDelayed(float p_fDelay)
	{

		if ((Time.timeScale == 0) && (p_fDelay > 0))
		{
			while (p_fDelay > 0)
			{
				yield return 0;
				p_fDelay -= Time.fixedDeltaTime;
			}

		}
		else
		{
			yield return new WaitForSeconds(p_fDelay);
		}

		UICamera.MouseOrTouch a_KeepTouch = UICamera.currentTouch;

		UICamera.currentTouch = m_CurrentTouch;
		s_CurrentTouch = m_CurrentTouch;
		s_CurrentCollider = m_CurentCollider;


		//		TADebug.Log(eTAG, "callbackExecute =" + this.name);
		callbackExecute();

		UICamera.currentTouch = a_KeepTouch;

	}

	void callbackExecute()
	{
		if (m_OnSubClicked != null)
		{
			m_OnSubClicked();
		}

		if (onClick != null)
		{
			onClick.Execute();
		}
	}
	void longPressCallbackExecute()
	{
		if (onLongPress != null)
		{
			onLongPress.Execute();
		}
	}

	public static bool isBusy
	{
		get { return (s_nBusy > 0); }
	}

	public static void setBusyTimer(float p_fBusyTime)
	{
		s_nBusy = p_fBusyTime;
	}

	/// <summary>
	/// ビジータイマーをクリアする
	/// </summary>
	public static void clearBusyTimer()
	{
		s_nBusy = 0;
	}

	/// <summary>
	/// ビジータイマーを経過時間分減らす
	/// ボタン同時押し対策
	/// 毎フレーム誰かがこれを呼ぶ必要がある。
	/// →基本はGameSystemから呼んでいる
	/// </summary>

	public static void clearOnClick()
	{
		if (s_nBusy > 0)
		{
			//			s_nBusy -= Time.fixedDeltaTime;
			s_nBusy -= Time.unscaledDeltaTime;
			if (s_nBusy <= 0)
			{
				s_nBusy = 0;
			}
		}
	}

	void OnPress(bool isPressed)
	{
		//		TADebug.Log(eTAG, "OnPress " + isPressed + " (" + gameObject.name + ")");

		// マスクターゲットが設定されている場合、マスクターゲットに自身が含まれていなければ無視。
		if ((s_MaskTargets.Count > 0) && !s_MaskTargets.Contains(this))
		{
			return;
		}

		if (this.enabled && !s_DisableAll)
		{
			if (
				(isPressed && !clickAtRelease) ||					// PUSH時にクリックする場合
				(!isPressed && clickAtRelease && m_bPressed)		// リリース時にクリックする場合
			)
			{
				if (!isBusy)
				{
					if (!m_bStarted) Start();

					//	効果音を鳴らす
					SoundHelper.eNo a_No = m_bSeDefault ? SoundHelper.eNo.Dummy : m_SeNo;
					if(a_No != SoundHelper.eNo.Dummy)	SoundHelper.instance.SePlay(a_No);

					//					TADebug.Log(eTAG, "m_bPressed " + gameObject.name);

					// ボタンアニメーション
					PressAnim_Start();
					// 長押しをキャンセル
					LongPress_CheckEnd();

					m_bPressed = true;
					m_DragMove = Vector2.zero;
					m_CurrentTouch = new UICamera.MouseOrTouch();
					m_CurrentTouch.current = UICamera.currentTouch.current;
					m_CurrentTouch.clickNotification = UICamera.currentTouch.clickNotification;
					m_CurrentTouch.clickTime = UICamera.currentTouch.clickTime;
					m_CurrentTouch.pos = UICamera.currentTouch.pos;

					m_CurentCollider = UICamera.lastHit.collider;

					StartCoroutine("AnimtypeClear_Exec");

					//			m_Animator.Play("Click");

					if ((depthOffset != 0) && !m_bAnimating)
					{
						m_bAnimating = true;
//						TANGUIUtil.DepthOffset(gameObject, depthOffset);
					}

					// リリース時にクリックする場合
					if (clickAtRelease)
					{
						_onClick();
						m_bPressed = false;
					}

				}
			}
			else
				if (!clickAtRelease)
				{
					if (!m_bStarted) Start();

					if (m_bPressed)
					{
						if (!isBusy)
						{
							_onClick();
						}
						m_bPressed = false;
					}
				}
				else
					if (clickAtRelease && isPressed)
					{
						if (!isBusy)
						{
							m_bPressed = true;
							if (!m_bLongPressed)
							{
								LongPress_CheckStart();
							}
						}
					}
		}
	}

	/// <summary>
	/// 押下アニメーションを再生する
	/// </summary>
	void PressAnim_Start()
	{
		if (animationType >= 0)
		{
			m_Animator.enabled = true;
			m_Animator.SetInteger("ptn", animationType);
			m_Animator.SetTrigger("on");
		}
		else
		{
			m_Animator.enabled = false;
		}
	}

	//
	// 長押しチェックを開始する
	//
	void LongPress_CheckStart()
	{
		if (longPress)
		{
			m_bLongPressed = false;
			PressAnim_Start();
			StartCoroutine("LongPress_Check");
		}
	}

	//
	// 長押しチェックを停止する
	//
	void LongPress_CheckEnd()
	{
		m_bLongPressed = false;
		StopCoroutine("LongPress_Check");
	}

	IEnumerator LongPress_Check()
	{
		float a_StartTime = 1.0f;

		while (a_StartTime > 0)
		{
			a_StartTime -= Time.fixedDeltaTime;
			yield return 0;
		}

		m_bLongPressed = true;
		m_bPressed = false;
		longPressCallbackExecute();
	}

	/*
		void OnHover (bool isOver)
		{
			if (enabled)
			{
				if (!mStarted) Start();
				TweenScale.Begin(tweenTarget.gameObject, duration, isOver ? Vector3.Scale(mScale, hover) : mScale).method = UITweener.Method.EaseInOut;
			}
		}
	 **/

	/*
		void OnSelect (bool isSelected)
		{
			TADebug.Log(eTAG, "OnSelect " + isSelected + " (" + gameObject.name + ")");
			if (enabled && (!isSelected || UICamera.currentScheme == UICamera.ControlScheme.Controller))
			{
				OnHover(isSelected);
			}
		}
	 */

	void OnDragStart()
	{
		//		TADebug.Log(eTAG, "OnDragStart " + " (" + gameObject.name + ")");
		m_DragMove = Vector2.zero;

		if (isBusy) return;

		if (clickAtRelease)
		{
			m_DragOutTime = 0;
		}

		// 長押しをキャンセルする
		if (longPress)
		{
			LongPress_CheckEnd();
		}
	}

	void OnDragOver(GameObject sender)
	{
		//		TADebug.Log(eTAG, "OnDragOver " + " (" + gameObject.name + ") <= " + sender.name);
	}

	void OnDragOut(GameObject sender)
	{
		//		TADebug.Log(eTAG, "OnDragOut " + " (" + gameObject.name + ") <= " + sender.name);

		if (clickAtRelease)
		{
			m_DragOutTime = Time.time;
		}
	}

	void OnDrag(Vector2 delta)
	{
		//		TADebug.Log(eTAG, "OnDrag " + delta.ToString() + " (" + gameObject.name + ")");
		m_DragMove += delta;
	}

	void OnDragEnd()
	{
		if (!this.enabled || s_DisableAll)
		{
			m_bPressed = false;
			return;
		}

		if (isBusy)
		{
			m_bPressed = false;
			return;
		}

		if (TouchDisable.isDisable)
		{
			m_bPressed = false;
			return;
		}

		if ((m_Collider != null) && !m_Collider.enabled)
		{
			m_bPressed = false;
			return;
		}


		//		TADebug.Log(eTAG, "OnDragEnd " + " (" + gameObject.name + ")");
		// 一定以上動いたら、押下をキャンセルする
		if (!clickAtRelease)
		{
			// PUSH時にクリックする場合
			if (m_DragMove.sqrMagnitude >= (10 * 10))
			{
				m_bPressed = false;

				// ドラッグ終了コールバック
				if (onDragEnd != null)
				{
					onDragEnd(m_DragMove);
				}
			}
		}
		else
		{
			// リリース時にクリックする場合
			if (m_DragOutTime < Time.time)
			{
				m_bPressed = false;
			}
		}

	}


	public bool isEnabled
	{
		get { return m_Button.isEnabled; }
		set { m_Button.isEnabled = value; }
	}

	/// <summary>
	/// ボタン（のコライダー）を無効にする
	/// </summary>
	public void forceDisable()
	{
		forceDisable(false);
	}
	public void forceDisable(bool p_bAtInputDisable)
	{
		//		TADebug.Log(eTAG, "forceDisable " + gameObject.name);

		if (m_Button == null) return;

		if (!p_bAtInputDisable)
		{
			//			m_Button.enabled = false;
			m_Button.SetState(UIButtonColor.State.Disabled, true);
		}

		if (m_Collider != null)
		{
			m_Collider.enabled = false;
		}

	}

	/// <summary>
	/// ボタン（のコライダー）を有効にする
	/// </summary>
	public void forceEnable()
	{
		//		TADebug.Log(eTAG, "forceEnable " + gameObject.name);

		UIButton a_B = GetComponent<UIButton>();
		a_B.state = UIButtonColor.State.Normal;
		a_B.defaultColor = Color.white;
		a_B.UpdateColor(false);

		a_B.enabled = true;
		a_B.SetState(UIButtonColor.State.Normal, true);

		if (m_Collider != null)
		{
			m_Collider.enabled = true;
		}

	}

	/// <summary>
	/// ボタンのアイドルモーションをＯＮ／ＯＦＦする
	/// </summary>
	/// <param name="p_bEnable"></param>
	public void idleEnable(bool p_bEnable)
	{
		enableIdle = p_bEnable;
		if (m_Animator != null)
		{
//			m_Animator.SetBool("idle", p_bEnable);
//			m_Animator.SetTrigger("change");
			if (p_bEnable && !m_Animator.enabled)
			{
				m_Animator.enabled = true;
			}
		}
	}

	void onAnimEnd()
	{
		//		TADebug.Log(eTAG, "onAnimEnd");

		if (m_bAnimating)
		{
//			TANGUIUtil.DepthOffset(gameObject, -depthOffset);
			m_bAnimating = false;
		}

	}

	/// <summary>
	/// アニメーションのフラグをクリアする
	/// </summary>
	/// <returns></returns>
	IEnumerator AnimtypeClear_Exec()
	{
		yield return new WaitForSeconds(0.1f);
		m_Animator.SetInteger("ptn", -1);

		// タイミングは用調整 TODO
		yield return new WaitForSeconds(0.2f);
		onAnimEnd();
	}


	/// <summary>
	/// ボタンの下地の色を変更する
	/// </summary>
	/// <param name="p_Col"></param>
	public void buttonColorSet(Color p_Col)
	{
		if (m_Button == null) return;

		m_Button.defaultColor = p_Col;
		m_Button.hover = p_Col;
		m_Button.pressed = p_Col;
	}

	/// <summary>
	/// ボタン無効時の下地の色を変更する
	/// </summary>
	/// <param name="p_Col"></param>
	public void buttonDisableColorSet(Color p_Col)
	{
		m_Button.disabledColor = p_Col;
	}

	/// <summary>
	/// ボタンの背景SPを変更する
	/// </summary>
	/// <param name="p_SpName"></param>
	public void buttonBgSpSet(string p_SpName)
	{
		uiButton.normalSprite = p_SpName;
		UISprite a_Sp = GameUtils.FindComponentByObjectName<UISprite>(this.gameObject, "Background");
		if (a_Sp != null)
		{
			a_Sp.spriteName = p_SpName;
		}
	}

	/// <summary>
	/// ボタンのラベルSPを変更する
	/// </summary>
	/// <param name="p_SpName"></param>
	public void buttonLabelSpSet(string p_SpName, bool p_bMakePixelPerfect = true)
	{
		UISprite a_Sp = GameUtils.FindComponentByObjectName<UISprite>(this.gameObject, "LabelSp");
		if (a_Sp != null)
		{
			a_Sp.spriteName = p_SpName;
			if (p_bMakePixelPerfect)
			{
				a_Sp.MakePixelPerfect();
			}
		}
	}

	/// <summary>
	/// ボタンのラベルテキストを変更する
	/// </summary>
	/// <param name="p_SpName"></param>
	public void buttonLabelTextSet(string p_Text)
	{
		UILabel a_Label = GameUtils.FindComponentByObjectName<UILabel>(this.gameObject, "Label");
		if (a_Label != null)
		{
			a_Label.text = p_Text;
		}
	}

	public UILabel	buttonLabelGet()
	{
		UILabel a_Label = gameObject.GetComponentInChildren<UILabel> ();
		return a_Label;
	}

	/// <summary>
	/// 他のボタンの押下状況を強制的にクリアする
	/// 同時押し対策
	/// </summary>
	static void ForceReleaseOtherButton()
	{
		UIButtonTween[] a_Buttons = GameObject.FindObjectsOfType<UIButtonTween>();
		if ((a_Buttons != null) && (a_Buttons.Length > 0))
		{
			foreach (UIButtonTween btw in a_Buttons)
			{
				btw.SendMessage("onForceRelease");
			}
		}
	}

	public void onForceRelease()
	{
		m_bPressed = false;
	}

	/// <summary>
	/// 全UIButtonTweenボタンの押下状況をキャンセルする
	/// </summary>
	public static void ForceReleaseAll()
	{
		ForceReleaseOtherButton();
		setBusyTimer(0.1f);
	}


	static bool s_DisableAll = false;

	/// <summary>
	/// 全UIButtonTweenボタンの有効／無効を切り替える
	/// trueで無効
	/// </summary>
	public static bool disableAll
	{
		set
		{
			s_DisableAll = value;
			ForceReleaseAll();
		}

		get
		{
			return s_DisableAll;
		}
	}


	/// <summary>
	/// マスクターゲットを追加する
	/// （マスクターゲットが設定されている場合、それ以外のボタンの入力を受け付けなくする）
	/// </summary>
	/// <param name="p_Sender"></param>
	public static void MaskTarget_Add(UIButtonTween p_Sender)
	{
		if (p_Sender == null) return;

		if (!s_MaskTargets.Contains(p_Sender))
		{
			s_MaskTargets.Add(p_Sender);
		}
	}
	public static void MaskTarget_Add(UIButtonTween[] p_Buttons)
	{
		if (p_Buttons == null) return;
		for (int i = 0; i < p_Buttons.Length; i++)
		{
			MaskTarget_Add(p_Buttons[i]);
		}
	}

	/// <summary>
	/// マスクターゲットを削除する
	/// </summary>
	/// <param name="p_Sender"></param>
	public static void MaskTarget_Remove(UIButtonTween p_Sender)
	{
		if (p_Sender == null) return;

		if (s_MaskTargets.Contains(p_Sender))
		{
			s_MaskTargets.Remove(p_Sender);
		}
	}
	public static void MaskTarget_Remove(UIButtonTween[] p_Buttons)
	{
		if (p_Buttons == null) return;
		for (int i = 0; i < p_Buttons.Length; i++)
		{
			MaskTarget_Remove(p_Buttons[i]);
		}
	}

	/// <summary>
	/// マスクターゲットを全て削除する
	/// </summary>
	public static void MaskTarget_Clear()
	{
		s_MaskTargets.Clear();
	}

	/// <summary>
	/// マスクターゲットの数を取得する
	/// </summary>
	public static int MaskTarget_Count()
	{
		return s_MaskTargets.Count;
	}

}
