﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class def : defSystem {

	/// <summary>
	/// 端末の標準サイズ（iPhone5）
	/// </summary>
	readonly public static			Vector2 c_ManualScreenSize    = new Vector2(640, 1136);

        /// <summary>
        /// 現在の端末サイズ
        /// </summary>
        /// <value>The size of the c screen.</value>
        public static                  Vector2 c_ScreenSize
        {
                get
                {
                        return new Vector2(Screen.width, Screen.height);        
                }
        }

	/// <summary>
	/// 最大音量
	/// </summary>
	readonly public static float	c_fMaxVolume = 1;

	/// <summary>
	/// 最小音量
	/// </summary>
	readonly public static float	c_fMInVolume = 0;

	/// <summary>
	/// サーバーURL（仮）
	/// </summary>
	readonly public static string	c_ConfigDataURL = "http://www.tryants.net/tomoda/";

	/// <summary>
	/// 太田胃散のサイトURL
	/// </summary>
	readonly public static string	c_OhtaIsanURL = "http://www.ohta-isan.co.jp/product/medicine/ohtaisan/";

	/// <summary>
	/// 太田胃にゃんのサイトURL
	/// </summary>
	readonly public static string	c_OhtaInyanURL = "http://www.ohta-isan.co.jp/ohtainyan/";

	/// <summary>
	/// 辞書データのパス
	/// </summary>
	readonly public static string	c_DicPath = Application.streamingAssetsPath + "/";

	static def s_Instance;
	public static def	instance
	{
		get { return s_Instance; }
	}

	/// <summary>
	/// 全シーンの共通処理
	/// </summary>
	public static void	CommonInit(System.Action p_CallBack)
	{
		if (s_Instance != null)
		{
			if (PanelManager.instance.dontDestroy) PanelManager.instance.Clean();
			if (p_CallBack != null)	p_CallBack();
			return;
		}

		def	a_This			= new GameObject("def").AddComponent<def>();
		a_This.hideFlags	= HideFlags.HideInHierarchy;	//	Hierarchyには表示しない
		s_Instance			= a_This;

		//	削除しない
		DontDestroyOnLoad(a_This.gameObject);

		Application.targetFrameRate = c_FPS;    //	デフォルトは６０
		Time.timeScale				= 1;		//	デフォルトは１

		// 言語設定
		const string	JP = "Japanese";
		const string	EN = "English";
		if (Application.systemLanguage == SystemLanguage.Japanese)
		{
			Localization.language = JP;
		}
		else
		{
			Localization.language = EN;
		}

		s_Instance.StartCoroutine("CommonInit_Exec", p_CallBack);
	}

	IEnumerator	CommonInit_Exec(System.Action p_Callback)
	{
		//	サウンド関連生成
		SoundManager.Create();
		SoundHelper.Create();

		//	サウンドシステムの準備が出来るまで待機
		while (!SoundHelper.instance.isReady)
		{
			yield return 0;
		}

        TADebug.Create();

        // UIRoot (PanelManager)
        GameObject	a_UIRoot = GameObject.FindGameObjectWithTag("uiRoot");
		a_UIRoot = ResourceManager.PrefabLoadAndInstantiate("Common/UIRootTemplate", Vector3.zero, null);

		//	uiRootの準備が出来るまで待機
		while(!PanelManager.isReady)
		{
			yield return 0;
		}

        //	タッチエフェクト
        TouchEffect.Create();

        //	タッチ制御
        TouchDisable.Create();

        //	プレイヤー情報
        PlayerInfo.Create();

		// ゲーム情報
		GameInfo.Create();

        //	リソースマネージャー
        ResourceManager.Create();

        //  WWWマネージャー
        WWWManager.Create();

        bool a_bWait = true;
        //	共通テーブル作成
        CreateTable(()=> 
            {
                a_bWait = false;
            }
        );
        while (a_bWait) yield return 0;

        if (p_Callback != null)	p_Callback();
	}

    [System.NonSerialized]
    public ConfigTable  configTable;

    public void CreateTable(System.Action p_Callback)
    {
        StartCoroutine(CreateTable_Exec(p_Callback));
    }

    /// <summary>
    /// 共通テーブルを作成する
    /// </summary>
    IEnumerator  CreateTable_Exec(System.Action p_Callback)
    {
        int a_nLoading = 0;

        //  コンフィグテーブルの生成とダウンロード
        a_nLoading++;
        configTable = ConfigTable.Create(true, ()=>
            {
                a_nLoading--;
            }
        );

        //  全テーブルのダウンロードが終了するまで待機
        while(a_nLoading > 0)
        {
            yield return 0;
        }

        if (p_Callback != null) p_Callback();
    }

    /// <summary>
    /// シーン共通で行う初期化が完了した時の処理
    /// </summary>
    public static void	CommonInit_Completed()
	{
		//	タッチ制限を解除
		TouchDisable.instance.SetDisable(false);
		//	フェードイン
		PanelManager.instance.fade.In(0, Color.clear);
	}

	Scene	m_PastScene;
	/// <summary>
	/// １つ前のシーン
	/// </summary>
	public Scene pastScene
	{
		get { return m_PastScene; }
	}

	Scene	m_CurrentScene;
	/// <summary>
	/// 現在のシーン
	/// </summary>
	public Scene	currentScene
	{
		get { return m_CurrentScene; }	
	}

	/// <summary>
	/// 指定したシーンに遷移する（一時的なシーンを含む）
	/// </summary>
	/// <param name="p_Scene"></param>
	/// <param name="p_bFade"></param>
	/// <returns></returns>
	public IEnumerator	SceneMove(Scene p_Scene, bool p_bFade, bool p_bShowInterstitial = false)
	{
		//	タッチを制限する
		TouchDisable.instance.SetDisable(true);

		float	a_fFadeTime = 0.5f;
		//	フェードアウト
		if(p_bFade)
		{
			PanelManager.instance.fade.Out(a_fFadeTime, Color.black, true);
		}

		//	フェードが終了するまで待機
		while (PanelManager.instance.fade.isFade)	yield return 0;

		//	タッチエフェクト削除
		TouchEffect.instance.AllDestroy();

		// SEを止める
		SoundHelper.instance.SeStop();

        //	一時的なシーンをはさむ
        SceneManager.LoadScene(c_SceneFileName[(int)Scene.TEMPORARY], LoadSceneMode.Additive);

		//	現在のシーンデータ
		UnityEngine.SceneManagement.Scene a_CurrentSceneData	= SceneManager.GetSceneByName(c_SceneFileName[(int)m_CurrentScene]);
		//	次のシーンデータ
		UnityEngine.SceneManagement.Scene a_NextSceneData		= SceneManager.GetSceneByName(c_SceneFileName[(int)Scene.TEMPORARY]);

		//	遷移する前に現在のシーンを１つ前のシーンとして保存
		m_PastScene = m_CurrentScene;

		//	遷移先のシーンに更新
		m_CurrentScene = p_Scene;

		//	シーンが変更され、次のシーンデータのロードが完了するまで待機
		while (!a_CurrentSceneData.isDirty && !a_NextSceneData.isLoaded)
		{
			yield return 0;
		}

		//	シーンのロード
		SceneManager.LoadScene(c_SceneFileName[(int)p_Scene], LoadSceneMode.Single);

		//	現在のシーンデータ
		a_CurrentSceneData	= SceneManager.GetSceneByName(c_SceneFileName[(int)m_CurrentScene]);
		//	次のシーンデータ
		a_NextSceneData		= SceneManager.GetSceneByName(c_SceneFileName[(int)p_Scene]);

		//	遷移先のシーンに更新
		m_CurrentScene = p_Scene;

		//	シーンが変更され、次のシーンデータのロードが完了するまで待機
		while(!a_CurrentSceneData.isDirty && !a_NextSceneData.isLoaded)
		{
			yield return 0;
		}

		yield return 0;
	}

	/// <summary>
	/// 言語環境による設定
	/// </summary>
	public static void Language_Set()
	{
		if (GameInfo.instance == null)	return;
		if (GameInfo.instance.language != GameInfo.eLanguage.NONE || 
			GameInfo.instance.language != GameInfo.eLanguage.MAX)
		{
			return;
		}

		//	言語設定
		SystemLanguage	a_Language = Application.systemLanguage;

		//	日本語
		if(a_Language == SystemLanguage.Japanese)
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.JPN);
		}
		else
		//	ポルトガル（ブラジル）
		if (a_Language == SystemLanguage.Portuguese)
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.PRT);
		}
		//	他は全て英語
		else
		{
			GameInfo.instance.Language_Set(GameInfo.eLanguage.ENG);
		}
	}

	void LateUpdate()
	{
		UIButtonTween.clearOnClick();
	}
}
