﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class PlayerInfo{

	public const string	eTAG = "PlayerInfo";

	/// <summary>
	/// 保存するキー
	/// </summary>
	public enum eKey : int
	{
		Max
	}

	/// <summary>
	/// プレイヤー情報データの配列
	/// </summary>
	public string[]	m_Info;

	[System.NonSerializedAttribute]
	static PlayerInfo s_Instance;
	/// <summary>
	/// シングルトンインスタンス
	/// </summary>
	public static PlayerInfo instance
	{
		get { return s_Instance; }
	}

	public static void Create()
	{
		if (s_Instance == null)
		{
			s_Instance = new PlayerInfo();
			s_Instance.Load();
		}
	}

	/// <summary>
	///	int型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public int getInt(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return 0;

		int a_nValue = 0;
		int.TryParse(m_Info[(int)p_Key], out a_nValue);
		return a_nValue;
	}

	/// <summary>
	/// int型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setInt(eKey p_Key, int p_nValue)
	{
		m_Info[(int)p_Key] = p_nValue.ToString();
		Save();
	}

	/// <summary>
	///	float型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public float getFloat(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return 0;

		float	a_fValue = 0;
		float.TryParse(m_Info[(int)p_Key], out a_fValue);
		return a_fValue;
	}

	/// <summary>
	/// float型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setFloat(eKey p_Key, float p_Value)
	{
		m_Info[(int)p_Key] = p_Value.ToString();
		Save();
	}

	/// <summary>
	/// string型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public string getStr(eKey p_Key)
	{
		return (string)m_Info[(int)p_Key];
	}

	/// <summary>
	/// string型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setStr(eKey p_Key, string p_Value)
	{
		m_Info[(int)p_Key] = p_Value;
		Save();
	}

	/// <summary>
	///	bool型の取得
	/// </summary>
	/// <param name="p_Key"></param>
	/// <returns></returns>
	public bool getBool(eKey p_Key)
	{
		if (string.IsNullOrEmpty(m_Info[(int)p_Key]))	return false;

		bool	a_bValue = false;
		bool.TryParse(m_Info[(int)p_Key], out a_bValue);
		return	a_bValue;
	}

	/// <summary>
	/// float型の設定
	/// </summary>
	/// <param name="p_Key"></param>
	/// <param name="p_Value"></param>
	public void setBool(eKey p_Key, bool p_bValue)
	{
		m_Info[(int)p_Key] = p_bValue.ToString();
		Save();
	}

	/// <summary>
	/// ロード
	/// </summary>
	public void Load()
	{
		string	a_Serialized = PlayerPrefs.GetString(eTAG);
//        a_Serialized = "";    //  データを初期化
		if (!string.IsNullOrEmpty(a_Serialized))
		{
			s_Instance = Deserialize(a_Serialized);

			// データが拡張されたら再生成する
			if (s_Instance.m_Info.Length < (int)eKey.Max)
			{
				string[]	a_KeepPref = s_Instance.m_Info;
				s_Instance.m_Info = new string[(int)(int)eKey.Max];
				for (int i = 0; i < a_KeepPref.Length; i++)
				{
					s_Instance.m_Info[i] = a_KeepPref[i];
				}
			}
		}
		else
		{
			// 初回起動時
			s_Instance = new PlayerInfo();
			s_Instance.DefaultSet();
			Save();
		}
	}

	/// <summary>
	/// デフォルト設定
	/// </summary>
	void	DefaultSet()
	{
		m_Info = new string[(int)eKey.Max];
		for (int i = 0; i < (int)eKey.Max; i++)
		{
			m_Info[i] = "";
		}
	}

	/// <summary>
	/// セーブ
	/// </summary>
	static void Save()
	{
		if (s_Instance != null)
		{
			string	a_SerializedStr = EasySerializer.SerializeObjectToString(s_Instance);
			PlayerPrefs.SetString(eTAG, a_SerializedStr);
		}
	}

	/// <summary>
	/// 自身をシリアル化して返す
	/// </summary>
	/// <returns></returns>
	public string Serialize()
	{
		string a_SerializedStr = EasySerializer.SerializeObjectToString(this);
		return a_SerializedStr;
	}

	/// <summary>
	/// セーブデータを戻す
	/// </summary>
	/// <param name="p_SerializedStr"></param>
	/// <returns></returns>
	static PlayerInfo Deserialize(string p_SerializedStr)
	{
		PlayerInfo	a_PlayerInfo = (PlayerInfo)EasySerializer.DeserializeObjectFromString(p_SerializedStr);
		return	a_PlayerInfo;
	}
}
