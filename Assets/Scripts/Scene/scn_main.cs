﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;

public class scn_main : MonoBehaviour
{
    const string eTAG = "scn_main";

    /// <summary>
    ///	ステート
    /// </summary>
    public enum eState : int
    {
        None,

        State_Init,
        State_Main,
        State_End,

        Max
    }

    //  ステート関連の処理
    #region

    /// <summary>
    ///	ステート名
    /// </summary>
    static Dictionary<eState, string> c_StateList;

    eState m_State;
    eState m_NextState;
    string m_Coroutine;

    /// <summary>
    /// ステート関連の初期化
    /// </summary>
    void State_Initialize()
    {
        //	ステートのリストを追加
        c_StateList = new Dictionary<eState, string>();
        for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
        {
            eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
            c_StateList.Add(a_State, a_State.ToString());
        }

        m_State = eState.None;
        m_NextState = eState.State_Init;
    }

    /// <summary>
    /// 次に呼び出すステートを指定する
    /// </summary>
    /// <param name="p_NextState"></param>
    /// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
    void State_Next(eState p_NextState, bool p_bContinus = false)
    {
        m_NextState = p_NextState;
        if (p_bContinus)
        {
            State_Check();
        }
    }

    /// <summary>
    /// ステートに変化が無いかチェックし、変化があったらステートを切り替える
    /// </summary>
    /// <returns></returns>
    bool State_Check()
    {
        if (m_NextState != m_State)
        {
            m_State = m_NextState;
            if (!string.IsNullOrEmpty(m_Coroutine))
            {
                StopCoroutine(m_Coroutine);
                m_Coroutine = null;
            }

            m_Coroutine = c_StateList[m_State];
            StartCoroutine(m_Coroutine);

            TADebug.Log(eTAG, "Call " + m_Coroutine + "()");

            return true;
        }

        return false;
    }
    #endregion

    /// <summary>
    /// 配置するプレハブの情報
    /// </summary>
    static PrefInstantiater.Layout[] c_Prefs =
        {
//		    new PrefInstantiater.Layout("Dummy",            "Dummy/Dummy",          false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero)),
            new PrefInstantiater.Layout("shotButton",       "Main/shotButton",      false,  0,  PanelManager.eLayer.Mid,   AlignManager.eAlign.Bottom,      new Vector3(   0,  80, 0)),
//        	new PrefInstantiater.Layout("backButton",		"Main/backButton",      false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Bottom,		new Vector3( 200,  90, 0)),
//        	new PrefInstantiater.Layout("settingsButton",	"Main/settingsButton",  false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Bottom,		new Vector3(-200,  80, 0)),
            new PrefInstantiater.Layout("BackButton",       "Common/BackButton",    false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Center,     new Vector3(-260, -60, 0)),
            new PrefInstantiater.Layout("HelpButton",       "Common/HelpButton",    false,  0,  PanelManager.eLayer.High,   AlignManager.eAlign.Center,     new Vector3( 260, -60, 0)),
            new PrefInstantiater.Layout("MotionButton",     "Main/MotionButton",    false,  0,  PanelManager.eLayer.Low,   AlignManager.eAlign.Center,      new Vector3(   0,   0, 0)),
        };
    PrefInstantiater m_Instantiator;

    [SerializeField]
    SmartARController m_Controller;

    List<SmartAREffector> m_EffectorList;

    List<UIButtonTween> m_GUIList;

    /// <summary>
    /// モデルリスト
    /// </summary>
    public List<SmartAREffector> modelList;

#if false
	/// <summary>
	/// 	モデルオブジェクトの子
	/// </summary>
	/// <value>The model child.</value>
	GameObject	modelChild
	{
		get
		{
			if (model == null)	return null;

			return model.transform.GetChild (0).gameObject;
		}
	}
#endif

    /// <summary>
    /// 認識していない
    /// </summary>
    bool isNoTracking
    {
        get
        {
            if (m_EffectorList == null || m_EffectorList.Count == 0) return false;

            foreach (SmartAREffector ef in m_EffectorList)
            {
                if (ef.result_.targetTrackingState_ == smartar.TargetTrackingState.TARGET_TRACKING_STATE_TRACKING)
                {
                    return false;
                }
            }

            return true;
        }
    }

    UITexture m_FrameTex;

    Vector3 m_DefaultPos;
    Vector3 m_DefaultAngle;

    // Use this for initialization
    void Start()
    {
        //  ステート関連の初期化
        State_Initialize();
        //	「State_Init」を呼び出す
        State_Next(m_NextState, true);
    }

    IEnumerator State_Init()
    {
        //	共通の初期化
        bool a_bWait = true;
        def.CommonInit(() =>
        {
            a_bWait = false;
            def.CommonInit_Completed();
        }
        );
        //	初期化が終了するまで待機
        while (a_bWait) yield return 0;

        TADebug.GUIDrawDelegateAppend(eTAG, OnGUITest);

        //	使用するプレハブをインスタンス化
        m_Instantiator = PrefInstantiater.Create();
        m_Instantiator.Build(c_Prefs);

//		m_DefaultPos	= modelChild.transform.localPosition;
//		m_DefaultAngle = modelChild.transform.localEulerAngles;

        //  エフェクトクラス生成
        RenderingEffect.Create();

        m_GUIList = new List<UIButtonTween>();

        //  撮影ボタン生成
        UIButtonTween a_Button = m_Instantiator.Get<UIButtonTween>("shotButton");
        a_Button.onClickEventSet(this, "ShotButtonDidPush", "", 0);
        m_GUIList.Add(a_Button);

        //  戻るボタン生成
        a_Button = m_Instantiator.Get<UIButtonTween>("BackButton");
        a_Button.onClickEventSet(this, "BackButtonDidPush", "", 0);
        m_GUIList.Add(a_Button);

        //      ヘルプボタン
        a_Button = m_Instantiator.Get<UIButtonTween>("HelpButton");
        a_Button.onClickEventSet(this, "HelpButtonDidPush", "", 0);
        m_GUIList.Add(a_Button);

        //      モーションボタン
        a_Button = m_Instantiator.Get<UIButtonTween>("MotionButton");
        a_Button.onClickEventSet(this, "MotionButtonDidPush", "", 0);
        BoxCollider a_Collider = a_Button.GetComponent<BoxCollider>();
        a_Collider.size = new Vector2(Screen.width, Screen.height);

        m_GUIList.Add(a_Button);
#if false
        m_Controller = ResourceManager.PrefabLoadAndInstantiate<SmartARController> ("Main/SmartARCamera");
        m_Controller.recognizerSettings_.targets = null;
        m_Controller.recognizerSettings_.targets = new SmartARController.TargetEntry[def.instance.configTable.dataCount];

        int a_nIndex = 0;
        foreach(ConfigTable.Data a_Data in def.instance.configTable.dataList)
        {
                SmartARController.TargetEntry   a_Target = new SmartARController.TargetEntry();

                a_Target.fileName   = a_Data.dicName;
                a_Target.id             = a_Data.targetID;

                m_Controller.recognizerSettings_.targets [a_nIndex] = a_Target;

                a_nIndex++;
        }

        foreach(SmartAREffector a_Model in modelList)
        {
                a_Model._SmartARCamera = m_Controller.GetComponent<Camera> ();
        }
#endif
        State_Next(eState.State_Main, true);
    }

    /// <summary>
    /// タイトル画面に戻るボタンが押された時の処理
    /// </summary>
    /// <param name="p_Sender"></param>
    public void BackButtonDidPush(UIButtonTween p_Sender)
    {
        StartCoroutine(def.instance.SceneMove(def.Scene.TITLE, true));
    }

    /// <summary>
    /// ヘルプボタンがおされた　時の処理
    /// </summary>
    /// <param name="p_Sender">P sender.</param>
    public void HelpButtonDidPush(UIButtonTween p_Sender)
    {
        GUI_ActiveSet(false);

        DescriptionPanel a_Panel = DescriptionPanel.Create(() =>
        {
            GUI_ActiveSet(true);
        },
        null);

        a_Panel.nextButton.gameObject.SetActive(false);
    }

    public void CloseButtonDidPush(UIButtonTween p_Sender)
    {
        Destroy(p_Sender.transform.parent.gameObject);
    }

    bool m_bAnimation = false;
    public void MotionButtonDidPush(UIButtonTween p_Sender)
    {
        foreach (SmartAREffector a_Model in modelList)
        {
            Animator a_Animator = a_Model.child.GetComponent<Animator>();
            if (a_Model.isTracking)
            {
                a_Animator.SetBool("param_idletorunning", m_bAnimation ^= true);
            }
            else
            {
                a_Animator.SetBool("param_idletorunning", false);
            }
        }
    }

    /// <summary>
    /// 撮影ボタンが押された時の処理
    /// </summary>
    /// <param name="p_Sender"></param>
    public void ShotButtonDidPush(UIButtonTween p_Sender)
    {
        //  ボタン類を非表示にする
        GUI_ActiveSet(false);

        //	撮影処理
        m_Controller.captureManager.Shot(
            //  撮影完了時に呼ばれる処理
            () =>
            {
                //  表示物を再表示する
                GUI_ActiveSet(true);
            }
        );
    }

    /// <summary>
    /// GUIの表示設定
    /// </summary>
    public void GUI_ActiveSet(bool p_bActive)
    {
        foreach (UIButtonTween a_Button in m_GUIList)
        {
            a_Button.gameObject.SetActive(p_bActive);
        }
    }

    bool m_bBeginTracking;

    public static smartar.Vector3 s_RotPosition;
    public static smartar.Quaternion s_RotRotation;

    /// <summary>
    /// 認識しているエフェクトを取得する
    /// </summary>
    /// <returns>The effect get.</returns>
    public List<SmartAREffector> TrackingEffect_Get()
    {
        List<SmartAREffector> list = new List<SmartAREffector>();

        foreach (SmartAREffector a_Effector in modelList)
        {
            if (a_Effector.isTracking) list.Add(a_Effector);
        }

        return list;
    }

    IEnumerator State_Main()
    {
        while (true)
        {
            foreach (SmartAREffector a_Model in modelList)
            {
                //  認識している
                if (a_Model.isTracking)
                {
                    //  認識開始時にモデルの位置をカメラに向ける
                    if (!a_Model.beginTracking || m_bDebugLoockAt)
                    {
#if false       //  カメラの方向にモデルを向ける
                        a_Model.beginTracking = true;

                        Vector3 a_RotPosition   = new Vector3(s_RotPosition.x_, s_RotPosition.y_, s_RotPosition.z_);
                        Vector3 a_RelativePos   = a_RotPosition - a_Model.child.transform.position;
                        Vector3 a_EulerAngle    = Quaternion.LookRotation (a_RelativePos, Vector3.up).eulerAngles;

                        a_EulerAngle.x  = 0;
                        a_EulerAngle.y -= a_Model.child.transform.eulerAngles.y;
                        a_EulerAngle.z  = 0;

                        a_Model.child.transform.localRotation = Quaternion.Euler(a_EulerAngle);

        ///     ログ
#if false
                        Debug.Log("モデル　RotPosition:" + a_RotPosition);
                        Debug.Log("モデル　eulerAngle:" + a_Model.child.transform.eulerAngles);
                        Debug.Log("モデル　localEulerAngles:" + a_Model.child.transform.localEulerAngles);
#endif
#endif

                        CSTransform.SetEuler_X(a_Model.child.transform, 270);
                        CSTransform.SetEuler_Y(a_Model.child.transform, 180);
                        CSTransform.SetPos_Z(a_Model.child.transform, -0.6f);
                        CSTransform.SetPos_X(a_Model.child.transform, 0.8f);
                    }
                }
                else
                {
                    a_Model.beginTracking = false;
                    a_Model.transform.localEulerAngles = Vector3.zero;
                    a_Model.child.transform.localEulerAngles = Vector3.zero;
                }
            }
            yield return 0;
        }

        State_Next(eState.State_End, true);
    }

    IEnumerator State_End()
    {
        yield return 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    // デバッグ用ＧＵＩ
#region FOR DEBUG

    Rect m_TestWinRect = new Rect(0, 130, 350, 350);
    Vector2 m_TestScrollPos = new Vector2(0, 0);

    bool m_bDebugLoockAt = false;

    public void OnGUITest(int p_nWindowID)
    {
        m_TestWinRect = GUILayout.Window(p_nWindowID, m_TestWinRect, OnGUITestWindow, eTAG);
    }

    public void OnGUITestWindow(int p_nWindowID)
    {
        if (modelList[0].child == null) return;

        GameObject modelChild = modelList[0].child;

        m_TestScrollPos = GUILayout.BeginScrollView(m_TestScrollPos, true, true, GUILayout.Width(320), GUILayout.Height(320));
        {
            GUILayout.BeginVertical("box");
            {
                GUILayout.Label("モデル");

                GUILayout.BeginVertical("box");
                {
                    GUILayout.Label("認識結果");

                    GUILayout.Label("モデル　EulerAngles：" + modelList[0].transform.eulerAngles);

                    GUILayout.Space(20);

                    if (GUILayout.Button("LookAt切替" + m_bDebugLoockAt.ToString()))
                    {
                        m_bDebugLoockAt ^= true;
                    }
                }
                GUILayout.EndVertical();

                GUILayout.Label("pos.x:" + modelChild.transform.localPosition.x);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("+0.1"))
                    {
                        CSTransform.AddPos_X(modelChild.transform, 0.1f);
                    }

                    if (GUILayout.Button("-0.1"))
                    {
                        CSTransform.AddPos_X(modelChild.transform, -0.1f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetPos_X(modelChild.transform, m_DefaultPos.x);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("pos.y:" + modelChild.transform.localPosition.y);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("+0.1"))
                    {
                        CSTransform.AddPos_Y(modelChild.transform, 0.1f);
                    }

                    if (GUILayout.Button("-0.1"))
                    {
                        CSTransform.AddPos_Y(modelChild.transform, -0.1f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetPos_Y(modelChild.transform, m_DefaultPos.y);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("pos.z:" + modelChild.transform.localPosition.z);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("+0.1"))
                    {
                        CSTransform.AddPos_Z(modelChild.transform, 0.1f);
                    }

                    if (GUILayout.Button("-0.1"))
                    {
                        CSTransform.AddPos_Z(modelChild.transform, -0.1f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetPos_Z(modelChild.transform, m_DefaultPos.z);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("rot.x:" + modelChild.transform.localEulerAngles.x);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("10"))
                    {
                        CSTransform.AddEuler_X(modelChild.transform, 10f);
                    }

                    if (GUILayout.Button("-10"))
                    {
                        CSTransform.AddEuler_X(modelChild.transform, -10f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetEuler_X(modelChild.transform, 0);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("rot.y:" + modelChild.transform.localEulerAngles.y);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("10"))
                    {
                        CSTransform.AddEuler_Y(modelChild.transform, 10f);
                    }

                    if (GUILayout.Button("-10"))
                    {
                        CSTransform.AddEuler_Y(modelChild.transform, -10f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetEuler_Y(modelChild.transform, 0);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("rot.z:" + modelChild.transform.localEulerAngles.z);
                GUILayout.BeginHorizontal("box");
                {
                    if (GUILayout.Button("10"))
                    {
                        CSTransform.AddEuler_Z(modelChild.transform, 10f);
                    }

                    if (GUILayout.Button("-10"))
                    {
                        CSTransform.AddEuler_Z(modelChild.transform, -10f);
                    }

                    if (GUILayout.Button("Default"))
                    {
                        CSTransform.SetEuler_Z(modelChild.transform, 0);
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndScrollView();

        GUI.DragWindow(new Rect(0, 0, m_TestWinRect.width, m_TestWinRect.height));
    }

#endregion

    /// <summary>
    /// 画像一覧を表示する
    /// </summary>
    public void ViewImages()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		using (AndroidJavaObject utils = new AndroidJavaObject("com.Intent.ViewGallery"))
		{
			utils.Call("showAlbum");
		}
#endif
    }
}
