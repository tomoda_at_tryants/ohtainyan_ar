﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class scn_title : MonoBehaviour
{
	const string eTAG = "scn_title";

	/// <summary>
	///	ステート
	/// </summary>
	public enum eState : int
    {
        None,

	    State_Init,
	    State_Main,
	    State_End,

	    Max
	}

//  ステート関連の処理
#region

	/// <summary>
	///	ステート名
	/// </summary>
	static Dictionary<eState, string> c_StateList;

	eState m_State;
	eState m_NextState;
	string m_Coroutine;

	/// <summary>
	/// ステート関連の初期化
	/// </summary>
	void State_Initialize()
	{
		//	ステートのリストを追加
		c_StateList = new Dictionary<eState, string>();
		for (int i = (int)eState.None + 1; i < (int)eState.Max; i++)
		{
	    		eState a_State = Enum.IsDefined(typeof(eState), i) ? (eState)i : eState.None;
	    		c_StateList.Add(a_State, a_State.ToString());
		}

		m_State = eState.None;
		m_NextState = eState.State_Init;
	}

	/// <summary>
	/// 次に呼び出すステートを指定する
	/// </summary>
	/// <param name="p_NextState"></param>
	/// <param name="p_bContinus">すぐにステートを切り替える場合はtrue</param>
	void State_Next(eState p_NextState, bool p_bContinus = false)
	{
        	m_NextState = p_NextState;
        	if (p_bContinus)
        	{
            		State_Check();
        	}
    	}

	/// <summary>
	/// ステートに変化が無いかチェックし、変化があったらステートを切り替える
	/// </summary>
	/// <returns></returns>
	bool State_Check()
	{
        	if (m_NextState != m_State)
        	{
            		m_State = m_NextState;
            		if (!string.IsNullOrEmpty(m_Coroutine))
            		{
	                	StopCoroutine(m_Coroutine);
                		m_Coroutine = null;
            		}

			m_Coroutine = c_StateList[m_State];
			StartCoroutine(m_Coroutine);

            		TADebug.Log(eTAG, "Call " + m_Coroutine + "()");

            		return true;
        	}
		return false;
	}
#endregion

	/// <summary>
	/// 配置するプレハブの情報
	/// </summary>
	static PrefInstantiater.Layout[] c_Prefs =
	{
//		new PrefInstantiater.Layout("Dummy",            "Dummy/Dummy",              false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero)),

		new PrefInstantiater.Layout("Title",            		"Title/Title",			false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(0, 400, 0)),
		new PrefInstantiater.Layout("ShootingButton",  	"Title/ShootingButton",	false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     Vector3.zero),
//		new PrefInstantiater.Layout("GalleryButton",    	"Title/GalleryButton",		false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(0, -250, 0)),
		new PrefInstantiater.Layout("WebButton_isan",   	"Title/WebButton",		false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(0, -400, 0)),
		new PrefInstantiater.Layout("WebButton_inyan",   "Title/WebButton",		false,  0,  PanelManager.eLayer.Mid,    AlignManager.eAlign.Center,     new Vector3(0, -250, 0)),
	};				
	PrefInstantiater m_Instantiator;

	// Use this for initialization
	void Start()
	{
		//  ステート関連の初期化
		State_Initialize();
		//	「State_Init」を呼び出す
		State_Next(m_NextState, true);
	}

	IEnumerator State_Init()
	{
		//	共通の初期化
		bool    a_bWait = true;
		def.CommonInit(() =>
		    {
                a_bWait = false;
                def.CommonInit_Completed();
        	}
        );

		//	初期化が終了するまで待機
		while (a_bWait) yield return 0;

        //	使用するプレハブをインスタンス化
        m_Instantiator = PrefInstantiater.Create();
        m_Instantiator.Build(c_Prefs);

        m_Instantiator.Get<GameObject>("Title");
        UIButtonTween   a_Button = m_Instantiator.Get<UIButtonTween>("ShootingButton");
        a_Button.onClickEventSet(this, "ShootingButtonDidPush", "", 0);

		a_Button = m_Instantiator.Get<UIButtonTween>("WebButton_isan");
		a_Button.onClickEventSet(this, "WebButtonDidPush", "", 0);
		a_Button.generalData.stringValue = "isan";

		a_Button = m_Instantiator.Get<UIButtonTween>("WebButton_inyan");
		a_Button.onClickEventSet(this, "WebButtonDidPush", "", 0);
		a_Button.buttonLabelTextSet ("太田胃にゃんサイト");
		a_Button.generalData.stringValue = "inyan";
		a_Button.buttonLabelGet ().MakePixelPerfect ();

    	State_Next(eState.State_Main, true);
	}

	public void ShootingButtonDidPush(UIButtonTween p_Sender)
	{
//		StartCoroutine(def.instance.SceneMove(def.Scene.MAIN, true));

	    DescriptionPanel.Create (null, ()=>
	        {
            	StartCoroutine(def.instance.SceneMove(def.Scene.MAIN, true));
	        }
	    );
	}

	public void GalleryButtonDidPush(UIButtonTween p_Sender)
	{
		ViewImages();
	}

	public void WebButtonDidPush(UIButtonTween p_Sender)
	{
		if (p_Sender.generalData.stringValue == "isan")
		{
			Application.OpenURL (def.c_OhtaIsanURL);	
		} 
		else 
		if (p_Sender.generalData.stringValue == "inyan")
		{
			Application.OpenURL (def.c_OhtaInyanURL);
		}
	}

    public void     NextButtonDidPush(UIButtonTween p_Sender)
    {
//      StartCoroutine(def.instance.SceneMove(def.Scene.MAIN, true));
    }

    public void     CloseButtonDidPush(UIButtonTween p_Sender)
    {
        Destroy (p_Sender.transform.parent.gameObject);
    }

    /// <summary>
    /// 画像一覧を表示する
    /// </summary>
    public void ViewImages()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        using (AndroidJavaObject utils = new AndroidJavaObject("com.Intent.ViewGallery"))
        {
            utils.Call("showAlbum");
        }
#endif
    }

    IEnumerator State_Main()
    {
        while (true)
        {
            yield return 0;
        }

        State_Next(eState.State_End, true);
    }

    IEnumerator State_End()
    {
        yield return 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
