﻿using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class ConfigTable : MonoBehaviour
{
	public static readonly string eTAG = "ConfigTable";

	public enum eParam : int
	{
		None = -1,

        ID,
        MarkerName,
        TargetID,
		DicName,
        ProductName,
		Discription,
	}

	public class Data
	{
		public string[]	paramStr;

        /// <summary>
		/// ID
		/// </summary>
		public string ID
        {
            get
            {
                string a_Param = paramStr[(int)eParam.ID];
                return a_Param;
            }
        }

        /// <summary>
        /// ターゲットID
        /// </summary>
        public string targetID
		{
			get
			{
				string	a_Param = paramStr[(int)eParam.TargetID];
				return	a_Param;
		    }
		}

		/// <summary>
		/// マーカー名
		/// </summary>
		public string markerName
		{
			get
			{
		        string	a_Param = paramStr[(int)eParam.MarkerName];
		        return	a_Param;
		    }
		}

		/// <summary>
		/// 辞書データ
		/// </summary>
		public string dicName
		{
			get
			{
		        string 	a_Param = paramStr[(int)eParam.DicName];
		        return 	a_Param;
			}
		}

        /// <summary>
        /// 商品名
        /// </summary>
        public string   productName
        {
            get
            {
                string  a_Param = paramStr[(int)eParam.ProductName];
                return a_Param;
            }
        }

		/// <summary>
		/// 説明文
		/// </summary>
		public string discription
		{
			get
			{
				string	a_Param = paramStr[(int)eParam.Discription];
		        return	a_Param;
		    }
		}

        /// <summary>
        /// 画像名（商品一覧用のテクスチャ）
        /// </summary>
        /// <value>The name of the tex.</value>
        public string   texName
        {
            get
            {
                return markerName;
            }
        }

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="p_Item"></param>
		public Data(string[] p_Item)
		{
			paramStr = p_Item;
		}
	}

	/// <summary>
	/// リスト
	/// </summary>
	public List<Data>	dataList;

    /// <summary>
    /// データ久安と
    /// </summary>
    /// <value>The data count.</value>
    public int      dataCount
    {
        get { return (dataList != null) ? dataList.Count : 0; }
    }

	public void Init()
	{
		dataList = new List<Data>();
    }

	/// <summary>
	/// 生成
	/// </summary>
	/// <returns></returns>
	public static ConfigTable Create(bool p_bDownload, System.Action p_Callback = null)
	{
		GameObject	a_Obj	= new GameObject();
		ConfigTable	me		= a_Obj.AddComponent<ConfigTable>();

        me.Init();

		//  ダウンロード処理開始
		me.Download(p_bDownload, () =>
			{
		        //  完了後の処理
		        if (p_Callback != null)
		        {
		            p_Callback();
		            Destroy(a_Obj);
		        }
			}
		);
		return me;
	}

	public void Download(bool p_bDownload, System.Action p_Callback)
	{
        if (p_bDownload)
        {
            StartCoroutine (Download_Exec (p_bDownload, p_Callback));
        } 
        else
        {
            if(p_Callback != null)  p_Callback();
        }
	}

	/// <summary>
	/// サーバーからダウンロードを行う
	/// </summary>
	/// <param name="p_Callback"></param>
	/// <returns></returns>
	IEnumerator   Download_Exec(bool p_bDownload, System.Action p_Callback)
	{
#if false
		if (p_bDownload)
		{
			//  「ConfigTable.csv」をダウンロードする
			string	a_URL	= string.Format ("{0}{1}.csv", def.c_ConfigDataURL, eTAG);
			WWW	www		= null;

			WWWManager.instance.Download (a_URL, eTAG, "", WWWManager.eType.None, "",
	    			//  ダウンロードが完了時に呼ばれるコールバック
				(p_WWW) =>{
					www = p_WWW;
				}
			);

			Debug.Log ("ダウンロード開始");

			//  ダウンロードが完了するまで待機
			while (!WWWManager.instance.isDone)
			{
				yield return 0;
			}

			Debug.Log ("ダウンロード完了");

			//  コンフィグテーブル生成（ダウンロード処理は行わない）
//	        ConfigTable a_Table = Create(false);

			char[]	a_Split 		= { '\n' };
			string[]	a_FieldString	= www.text.Split (a_Split);

			int a_nIndex = 0;
			foreach (string a_Str in a_FieldString)
			{
				if (!string.IsNullOrEmpty (a_Str) && a_nIndex > 0)
				{
					a_Split = new char[] { ',' };
					string[] a_Item = a_Str.Split (a_Split);

					//  コンフィグデータを生成し、リストに追加
					Data a_Data = new Data (a_Item);
					dataList.Add (a_Data);
				}
				a_nIndex++;
			}

			for (int i = 0; i < dataList.Count; i++)
			{
				//  ダウンロードしてきたデータ
				Data a_NewData = dataList [i];

				//  ダウンロードしたデータをリストに追加
				def.instance.configTable.dataList.Add (a_NewData);

				//  既にバージョンリストに含まれている設定データか
//				bool a_bContains = GameInfo.instance.configVersionList.ContainsKey (a_NewData.ID);
				//  バージョンアップか
//				bool a_bVersionUp = (a_bContains) ? (GameInfo.instance.configVersionList [a_NewData.ID] < a_NewData.version) : false;

				//  新規データ
				//  設定バージョンが上がっている時はリソースを更新する
//				if (!a_bContains || a_bVersionUp)
				{
					//  バージョンリストを更新する
//					GameInfo.instance.ConfigVersion_Set (a_NewData.ID, a_NewData.version);

					//  フレーム画像のダウンロード
//					a_URL = string.Format ("{0}{1}.png", def.c_ConfigDataURL, a_NewData.frameName);
//					WWWManager.instance.Download (a_URL, a_NewData.frameName, def.c_FrameTexPath, WWWManager.eType.Byte);

					//  マーカー画像のダウンロード
					a_URL = string.Format ("{0}{1}.png", def.c_ConfigDataURL, a_NewData.markerName);
					WWWManager.instance.Download (a_URL, a_NewData.markerName, def.c_MarkerTexPath, WWWManager.eType.Byte);

					//  辞書データのダウンロード
					a_URL = string.Format ("{0}{1}.dic", def.c_ConfigDataURL, a_NewData.dicName);
					WWWManager.instance.Download (a_URL, a_NewData.dicName, def.c_DicPath, WWWManager.eType.Byte, ".dic");
				}
			}

/*
			//  ダウンロードが完了するまで待機
			while (WWWManager.instance.downloadNum > 0)
			{
				yield return 0;
			}
*/
		}
		else
#endif

        TextAsset       a_Asset     = Resources.Load<TextAsset> ("CSV/ConfigTable");
        StringReader    a_Reader    = new StringReader (a_Asset.text);

        int     a_nLIne = 0;
        char[]  a_Split = { ',' };
        while(a_Reader.Peek() > -1)
        {
            //      最初の行は飛ばす
            if(a_nLIne == 0)
            {
                a_nLIne++;
                a_Reader.ReadLine ();
            }
            string      a_Line      = a_Reader.ReadLine();
            string[]    a_LineArray = a_Line.Split(a_Split);

            //      コンフィグデータを生成し、リストに追加
            Data    a_Data = new Data(a_LineArray);
            dataList.Add(a_Data);

            a_nLIne++;

            yield return 0;
        }

        if(p_Callback != null)  p_Callback();

		yield return 0;
	}
}
