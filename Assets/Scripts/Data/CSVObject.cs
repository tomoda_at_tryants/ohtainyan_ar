﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVObject {


	static string eTAG = "CSVObject";

	List<List<string>> value;

	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="p_Text"></param>
	/// <returns></returns>
	public static CSVObject Create(string p_Text)
	{
		CSVObject a_This = new CSVObject();
		a_This.Init(p_Text);

		return a_This;
	}

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="p_Text"></param>
	void Init(string p_Text)
	{
		value = new List<List<string>>();

		string[] a_Lines = p_Text.Split('\r');
		foreach(string a_Line0 in a_Lines)
		{
			string a_Line = a_Line0.Trim();
			if (!string.IsNullOrEmpty(a_Line))
			{
				List<string> a_Columns = new List<string>(a_Line.Split(','));
				value.Add(a_Columns);
			}
		}
	}

	/// <summary>
	/// 行数を返す
	/// </summary>
	/// <returns></returns>
	public int	LineCountGet()
	{
		return value.Count;
	}

	/// <summary>
	/// 指定行の要素数を返す
	/// </summary>
	/// <param name="p_nLineNo"></param>
	/// <returns></returns>
	public int RowCountGet(int p_nLineNo)
	{
		if ((p_nLineNo<0) || (p_nLineNo >= value.Count))
		{
			TADebug.LogError(eTAG, "RowCountGet line no error. " + p_nLineNo);
			return 0;
		}

		return value[p_nLineNo].Count;
	}

	/// <summary>
	/// 指定行の配列を返す
	/// </summary>
	/// <param name="p_nLineNo"></param>
	/// <returns></returns>
	public List<string>		LineDataGet(int p_nLineNo)
	{
		if ((p_nLineNo<0) || (p_nLineNo >= value.Count))
		{
			TADebug.LogError(eTAG, "LineDataGet line no error. " + p_nLineNo);
			return null;
		}

		return value[p_nLineNo];
	}


}
