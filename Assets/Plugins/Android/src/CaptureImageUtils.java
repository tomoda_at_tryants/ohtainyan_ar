package com.sony.smartar.utils;

import java.io.File;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaActionSound;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;

public class CaptureImageUtils {

    private static final String SMART_CAPTURE_DIRECTORY = "/smartar/capture/";
    
    private CaptureImageUtils() {
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void playSystemShutterSound() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) { return; }

        final MediaActionSound mas = new MediaActionSound();
        mas.load(MediaActionSound.SHUTTER_CLICK);
        new Handler(UnityPlayer.currentActivity.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mas.play(MediaActionSound.SHUTTER_CLICK);
            }
        }, 100);
    }

    public static String moveToExternalDir(String path) {
        final File captureImageFile = new File(path);
        if (!captureImageFile.exists()) { return null; }

        final String outputPath = Environment.getExternalStorageDirectory().getAbsolutePath() + SMART_CAPTURE_DIRECTORY;
        if (!new File(outputPath).exists()) {
            new File(outputPath).mkdirs();
        }
        final File dest = new File(outputPath, captureImageFile.getName());
        final boolean success = captureImageFile.renameTo(dest);

        return success ? dest.getAbsolutePath() : null;
    }

    public static void scanCaptureImage(String path) {
        final Context context = UnityPlayer.currentActivity.getApplicationContext();
        final String[] paths = new String[] { path };
        final String[] mimeTypes = new String[] { "image/*" };
        MediaScannerConnection.scanFile(context, paths, mimeTypes, new MediaScannerConnection.OnScanCompletedListener() {
            @Override
            public void onScanCompleted(String path, Uri uri) {
                UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Completed capture image.", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
