
//  クリップボードに指定したテキストをコピーする
extern "C" void _SetClipboard(const char* p_ExportData)
{
    UIPasteboard.generalPasteboard.string = [[NSString alloc] initWithUTF8String:p_ExportData];
}

//  クリップボードにコピーされたテキストを取得する
extern "C" char* _GetClipboard()
{
    return (char*)[UIPasteboard.generalPasteboard.string UTF8String];
}
