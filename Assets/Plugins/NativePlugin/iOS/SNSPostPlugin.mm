#if UNITY_VERSION <= 434
#import "iPhone_View.h"
#endif
#import <Social/Social.h>

//  Line投稿
//  画像とメッセージは同時に指定できない
extern "C" void _PostLine(const char *textureURL){
    NSString *_textureURL = [NSString stringWithUTF8String:textureURL ? textureURL : ""];
    NSString *contentType;
    NSString *contentKey;
    
    UIImage *image = [UIImage imageWithContentsOfFile:_textureURL];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.image = image;
    contentType = @"image";
    contentKey = pasteboard.name;
        
    NSURL *lineUrl = [NSURL URLWithString:[NSString stringWithFormat:@"line://msg/%@/%@", contentType, contentKey]];
        
    [[UIApplication sharedApplication] openURL:lineUrl];
}

//  Twitter投稿
//  画像とメッセージ両方指定可能
extern "C" void _PostTwitter(const char *textureURL, const char* text)
{
    NSString *_text = [[NSString alloc] initWithUTF8String : text];
    NSString *_url  = @"";
    NSString *_textureURL = [NSString stringWithUTF8String:textureURL ? textureURL : ""];
    
    SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    composeViewController.completionHandler = ^(SLComposeViewControllerResult res)
    {
        [composeViewController dismissViewControllerAnimated:YES completion:nil];
    };
    
    if ([_text length] != 0) {
        [composeViewController setInitialText:_text];
    }
    if ([_url length] != 0) {
        [composeViewController addURL:[NSURL URLWithString:_url]];
    }
    if ([_textureURL length] != 0) {
        UIImage *image = [UIImage imageWithContentsOfFile:_textureURL];
        if (image != nil) {
            [composeViewController addImage:image];
        }
    }
    
    [UnityGetGLViewController() presentViewController:composeViewController animated:YES completion:nil];
}

UIDocumentInteractionController * interactionController;

//  Instagram投稿
//  メッセージは指定できないので代用としてクリップボードにコピーしてユーザーにペーストしてもらうようにする
extern "C" void _PostInstagram(const char *textureURL)
{
    NSString *_textureURL = [NSString stringWithUTF8String:textureURL ? textureURL : ""];
    
    UIImage *image = [UIImage imageWithContentsOfFile:_textureURL];
    NSData  *imageData = UIImagePNGRepresentation(image);
    
    NSString    *filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/image.igo"];
    
    [imageData writeToFile:filePath atomically:YES];
    NSURL   *fileURL = [NSURL fileURLWithPath:filePath];
    
    interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    interactionController.UTI = @"com.instagram.exclusivegram";

    UIView *currentView = UnityGetGLView();

    [interactionController presentOpenInMenuFromRect:currentView.frame inView:currentView animated:YES];
}

//  Facebook投稿
//  メッセージは指定できないので代用としてクリップボードにコピーしてユーザーにペーストしてもらうようにする
extern "C" void _PostFacebook(const char *textureURL)
{
    NSString *_textureURL = [NSString stringWithUTF8String:textureURL ? textureURL : ""];
    
    UIImage *image = nil;
    
    if ([_textureURL length] != 0) {
        image = [UIImage imageWithContentsOfFile:_textureURL];
    }
    
    SLComposeViewController *slcomposeViewComtroller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    //  テキスト
    [slcomposeViewComtroller setInitialText:@"イニシャルテキスト"];
    //  画像
    [slcomposeViewComtroller addImage:image];
    
    //  処理終了後に呼ばれるコールバックを指定する
    [slcomposeViewComtroller setCompletionHandler:^(SLComposeViewControllerResult result)
     {
         switch(result)
         {
             case SLComposeViewControllerResultDone:
             {
                 NSLog(@" facebookDone");
             }
                 break;
                 
             case SLComposeViewControllerResultCancelled:
             {
                 NSLog(@"facebook cancel");
             }
                 break;
         }
     }
     ];
    
    //  表示する
    [UnityGetGLViewController() presentViewController:slcomposeViewComtroller animated:YES completion:nil];
}
