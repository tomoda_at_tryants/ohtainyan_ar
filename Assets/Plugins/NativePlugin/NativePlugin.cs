﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;   //  DllImport
using System;

public class NativePlugin : MonoBehaviour
{
    /// <summary>
    /// タグ
    /// </summary>
    const string eTAG = "NativePlugin";

    /// <summary>
    /// UnityPlayerActivity(Android)
    /// </summary>
    static AndroidJavaClass UnityPlayerActivity
    {
        get
        {
#if !UNITY_ANDROID
            return null;
#else
            return new AndroidJavaClass("com.unity3d.player.UnityPlayer");
#endif
        }
    }

    /// <summary>
    /// クリップボード関連
    /// </summary>
    public class ClipboardManager
    {
        /// <summary>
        /// クリップボードに張り付ける
        /// </summary>
        /// <param name="p_ExportData"></param>
        public static void Set(string p_ExportData)
        {
            if (string.IsNullOrEmpty(p_ExportData) || Application.isEditor) return;

#if UNITY_ANDROID
            using (AndroidJavaClass jc = UnityPlayerActivity)
            {
                using (AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (AndroidJavaObject clipboardManager = activity.Call<AndroidJavaObject>("getSystemService", "clipboard"))
                    {
                        clipboardManager.Call("setText", p_ExportData);
                    }
                }
            }
#elif UNITY_IOS
		    _SetClipboard(p_ExportData);
#endif
        }

        /// <summary>
        /// クリップボードのデータを取得する
        /// </summary>
        /// <returns></returns>
        public static string Get()
        {
            if (Application.isEditor) return "";

            string a_Data = "";
#if UNITY_ANDROID
            using (AndroidJavaClass jc = UnityPlayerActivity)
            {
                using (AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (AndroidJavaObject clipboardManager = activity.Call<AndroidJavaObject>("getSystemService", "clipboard"))
                    {
                        //  文字列でない場合もあるのでtry～catchする
                        try
                        {
                            a_Data = clipboardManager.Call<string>("getText");
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            }
#elif UNITY_IOS
        a_Data = _GetClipboard();
#endif

            return a_Data;
        }

        [DllImport("__Internal")]
        static extern void _SetClipboard(string p_ExportData);  //  NativePlugin_iOSに記述

        [DllImport("__Internal")]
        static extern string _GetClipboard();                   //  NativePlugin_iOSに記述
    }

    /// <summary>
    /// SNS投稿関連
    /// </summary>
    public class SNSPostManager
    {
        /// <summary>
        /// SNSの種類
        /// </summary>
        public enum eSNS
        {
            None = -1,

            LINE,
            Twitter,
            Instagram,
            Facebook,

            Max
        }

        public static eSNS getSNSType(string p_SNS)
        {
            try
            {
                return (eSNS)Enum.Parse(typeof(eSNS), p_SNS);
            }
            catch (Exception e)
            {
                return eSNS.None;
            }

            return eSNS.None;
        }

        /// <summary>
        /// パッケージ名リスト
        /// </summary>
        public static Dictionary<eSNS, string> m_PackageNameList = new Dictionary<eSNS, string>()
        {
            { eSNS.None,        "" },
            { eSNS.LINE,        "jp.naver.line.android"},
            { eSNS.Twitter,     "com.twitter.android" },
            { eSNS.Instagram,   "com.instagram.android" },
            { eSNS.Facebook,    "com.facebook.katana" }
        };

        /// <summary>
        /// 投稿処理
        /// </summary>
        public static void Post(eSNS p_SNS, string p_TexturePath, string p_Text)
        {
            if (p_SNS == eSNS.None || string.IsNullOrEmpty(p_TexturePath) || Application.isEditor) return;

#if UNITY_ANDROID
            switch (p_SNS)
            {
                //  LineのみURLを指定し、起動する
                case eSNS.LINE:
                    {
                        string a_Url = "line://msg/image/" + p_TexturePath;
                        Application.OpenURL(a_Url);
                    }
                    break;

                //  LINE以外はパッケージ名を変えるだけ
                default:
                    {
                        Post_Android(p_SNS, p_TexturePath, p_Text);
                    }
                    break;
            }
#elif UNITY_IOS
            switch (p_SNS)
            {
                case eSNS.LINE:
                    {
                        _PostLine(p_TexturePath);
                    }
                    break;

                case eSNS.Twitter:
                    {
					    _PostTwitter(p_TexturePath, p_Text);
                    }
                    break;

                case eSNS.Instagram:
                    {
					    _PostInstagram(p_TexturePath);
                    }
                    break;

                case eSNS.Facebook:
                    {
                        _PostFacebook(p_TexturePath);
                    }
                    break;
            }
#endif
        }

        /// <summary>
        /// Android版の投稿処理
        /// </summary>
        private static void Post_Android(eSNS p_SNS, string p_TexturePath, string p_Text)
        {
            string a_PackageName = m_PackageNameList[p_SNS];

            using (AndroidJavaClass jcUnityPlayer = UnityPlayerActivity)
            {
                using (AndroidJavaObject joActivity = jcUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (var intent = new AndroidJavaObject("android.content.Intent"))
                    {
                        intent.Call<AndroidJavaObject>("setAction", "android.intent.action.SEND");
                        intent.Call<AndroidJavaObject>("setType", "image/*");

                        intent.Call<AndroidJavaObject>("setPackage", a_PackageName);

                        using (var uri = new AndroidJavaClass("android.net.Uri"))
                        {
                            using (var file = new AndroidJavaObject("java.io.File", p_TexturePath))
                            {
                                intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.STREAM", uri.CallStatic<AndroidJavaObject>("fromFile", file));

                                //  Twitterのみテキストと画像を同時に投稿できる
                                if (p_SNS == eSNS.Twitter)
                                {
                                    //  テキスト追加
                                    intent.Call<AndroidJavaObject>("putExtra", "android.intent.extra.TEXT", p_Text);

                                }

                                using (var chooser = intent.CallStatic<AndroidJavaObject>("createChooser", intent, ""))
                                {
                                    chooser.Call<AndroidJavaObject>("putExtra", "android.inent.extra.EXTRA_INITIAL_INTENTS", intent);
                                    joActivity.Call("startActivity", chooser);
                                }
                            }
                        }
                    }
                }
            }
        }

        [DllImport("__Internal")]
        static extern void _PostLine(string textureUrl);

        [DllImport("__Internal")]
        static extern void _PostTwitter(string textureUrl, string text);

        [DllImport("__Internal")]
        static extern void _PostInstagram(string textureUrl);

        [DllImport("__Internal")]
        static extern void _PostFacebook(string textureURL);
    }

    /// <summary>
    /// アルバム関連（Android:ギャラリー　iOS:カメラロール）
    /// </summary>
    public class ImageAlbumManager
    {
        /*
            各ライブラリは「NativePlugin/ImageAlbumManager」に入っている
        */

        public class ActivityListener : AndroidJavaProxy
        {
            public Action<int, int, AndroidJavaObject> onActivityResultEvent;

            public ActivityListener() : base("com.tryants.utils.GalleryListener")
            {
            }

            public void onRestart()
            {
            }

            public void onStart()
            {
            }

            public void onResume()
            {
            }

            public void onPause()
            {
            }

            public void onStop()
            {
            }

            public void onActivityResult(int requestCode, int resultCode, AndroidJavaObject data)
            {
                onActivityResultEvent(requestCode, resultCode, data);
            }
        }

        static ActivityListener m_Listener;

        /// <summary>
        /// 画像を選択する
        /// </summary>
        public static void SelectImage(System.Action<string, int> p_LoadTextureCallback)
        {
            if (Application.isEditor)
            {
                p_LoadTextureCallback("", 0);
                return;
            }
#if UNITY_ANDROID
            using (AndroidJavaClass javaClass = new AndroidJavaClass("com.tryants.utils.GalleryActivity"))
            {
                javaClass.CallStatic("SetListener", m_Listener = new ActivityListener());
                javaClass.CallStatic("OpenGallery");
            }

            //  ギャラリーで画像を選択した後に呼ばれる処理
            m_Listener.onActivityResultEvent = (int requestCode, int resultCode, AndroidJavaObject data) =>
            {
                var d = data.Call<AndroidJavaObject>("getData");
                using (AndroidJavaClass dd = new AndroidJavaClass("android.provider.DocumentsContract"))
                {
                    var strDocId = dd.CallStatic<string>("getDocumentId", d);
                    var strSplittedDocId = strDocId.Split(":"[0]);
                    var strId = strSplittedDocId[strSplittedDocId.Length - 1];

                    //  Unityのアクティビティ取得
                    using (var unityPlayer = UnityPlayerActivity)
                    {
                        using (var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                        {
                            var r = activity.Call<AndroidJavaObject>("getContentResolver");
                            var c = r.Call<AndroidJavaObject>("query",
                                new AndroidJavaClass("android.provider.MediaStore$Images$Media").GetStatic<AndroidJavaObject>("EXTERNAL_CONTENT_URI"),
                                new string[]
                                {
                                new AndroidJavaClass("android.provider.MediaStore$MediaColumns").GetStatic<string>("DATA")
                                },
                                "_id=?",
                                new string[] { strId },
                                null
                            );

                            //  画像のパス取得
                            if (c.Call<bool>("moveToFirst"))
                            {
                                string p_ImagePath = c.Call<string>("getString", 0);
                                //  イメージの回転角度をExifから取得する
                                int a_nDegree = GetOrientation(p_ImagePath);

                                //  画像がロードした時に呼ばれるコールバック
                                p_LoadTextureCallback(p_ImagePath, a_nDegree);
                            }

                            c.Call("close");
                        }
                    }
                }
            };

#elif UNITY_IOS
            //  画像の選択が完了した際に呼ばれるコールバックを設定
		    GameObject	a_Obj = new GameObject("Unimgpicker");
		    Unimgpicker a_Picker = a_Obj.AddComponent<Unimgpicker>();
            a_Picker.Completed += 
			    (string path, int degree) =>
                {
				    p_LoadTextureCallback(path, degree);
				    Destroy(a_Obj);
                };

            a_Picker.Show("Select Image", "unimgpicker", 1024);
#endif
        }

        /// <summary>
        /// 指定した画像の角度を返す
        /// </summary>
        /// <param name="p_ImagePath"></param>
        static int GetOrientation(string p_ImagePath)
        {
            int a_nDegree = -1;

            if (string.IsNullOrEmpty(p_ImagePath)) return a_nDegree;

            int a_nOrientation = -1;
            try
            {
                using (AndroidJavaObject a_Interface = new AndroidJavaObject("android.media.ExifInterface", p_ImagePath))
                {
                    a_nOrientation = a_Interface.Call<int>("getAttributeInt", "Orientation", 1);
                    switch (a_nOrientation)
                    {
                        //  回転していない
                        case 6:
                            {
                                a_nDegree = 270;
                            }
                            break;

                        //  
                        case 3:  //  １８０度回転している
                            {
                                a_nDegree = 180;
                            }
                            break;

                        case 8:     //  ２７０度回転している
                            {
                                a_nDegree = 90;
                            }
                            break;

                        default:
                            {
                                a_nDegree = 0;
                            }
                            break;
                    }
                }
            }
            catch (AndroidJavaException ex)
            {
                return a_nDegree;
            }

            return a_nDegree;
        }

        /// <summary>
        /// iOS版カメラロール表示クラス
        /// </summary>
        public class Unimgpicker : MonoBehaviour
        {
            public delegate void ImageDelegate(string path, int degree);
            public delegate void ErrorDelegate(string message);

            public event ImageDelegate Completed;
            public event ErrorDelegate Failed;

            private int degree;
            bool m_bCalled;

            public void Show(string title, string outputFileName, int maxSize)
            {
                Unimgpicker_show(title, outputFileName, maxSize);
            }

            private void OnComplete(string path)
            {
                StartCoroutine(Complete_Coroutine(path));
            }

            IEnumerator Complete_Coroutine(string path)
            {
                while (!m_bCalled) yield return 0;

                ImageDelegate a_Handler = Completed;
                if (a_Handler != null) a_Handler(path, this.degree);
            }

            private void GetOrientation(string orientation)
            {
                switch (int.Parse(orientation))
                {
                    case 3:	//	縦
                        {
                            this.degree = -90;
                        }
                        break;

                    case 2:
                        {
                            this.degree = 90;
                        }
                        break;

                    case 1:
                        {
                            this.degree = 180;
                        }
                        break;

					case 0:
						{
							this.degree = 0;
						}
						break;

                    default:
                        {
//                            this.degree = -90;
                        }
                        break;
                }

                m_bCalled = true;
            }

            private void OnFailure(string path)
            {
                ErrorDelegate a_Handler = Failed;
                if (a_Handler != null) a_Handler(path);
            }

            [DllImport("__Internal")]
            private static extern void Unimgpicker_show(string title, string outputFileName, int maxSize);
        }
    }

    public class CheckInstalledManager
    {
        /// <summary>
        /// 指定したパッケージ名のアプリがインストールされているか確認する（Androidのみ可能）
        /// </summary>
        /// <param name="p_PackageName"></param>
        /// <returns></returns>
        public static bool Check(string p_PackageName)
        {
            if (string.IsNullOrEmpty(p_PackageName) || Application.isEditor) return false;

            //  Android以外は対応外
#if !UNITY_ANDROID
            return true;
#endif

            bool a_bResult = false;

            AndroidJNI.AttachCurrentThread();
            AndroidJNI.PushLocalFrame(0);

            try
            {
                using (AndroidJavaClass jc = UnityPlayerActivity)
                {
                    using (AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity"))
                    {
                        //  PackageManagerを取得
                        using (AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager"))
                        {
                            //  インストールアプリ情報リストを取得
                            using (AndroidJavaObject applicationInfoList = packageManager.Call<AndroidJavaObject>("getInstalledApplications", 0))
                            {
                                //  リストの要素数取得
                                int a_nListCount = applicationInfoList.Call<int>("size");
                                //  対象のパッケージ名があるか探す
                                for (int i = 0; i < a_nListCount; i++)
                                {
                                    //  パッケージ名を取得
                                    string name = GetPackageName(applicationInfoList, i);
                                    //  パッケージ名を比較
                                    if (!string.IsNullOrEmpty(name) && p_PackageName.Equals(name))
                                    {
                                        //  インストールされている
                                        a_bResult = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                AndroidJNI.PopLocalFrame(IntPtr.Zero);
            }

            return a_bResult;
        }

        /// <summary>
        /// インストールアプリ情報リストから指定インデックスのパッケージ名を取得する
        /// </summary>
        static string GetPackageName(AndroidJavaObject joApplicationInfoList, int index)
        {
            string pName = "";

            AndroidJNI.AttachCurrentThread();   //  Java(Dalvik)VMを現行のスレッドにアタッチする
            /*
                JNI
                javaとネイティブコード（C/C++）を連携するための仕組み。共有ライブラリからの動的読み込みをサポートしている
            */
            AndroidJNI.PushLocalFrame(0);       //  新しいローカル参照ふれーむ　を、最低でも設定した数作成する
            try
            {
                // アプリ情報取得
                using (AndroidJavaObject joApplicationInfo = joApplicationInfoList.Call<AndroidJavaObject>("get", index))
                {
                    // パッケージ名を取得
                    pName = joApplicationInfo.Get<string>("packageName");
                }
            }
            catch (System.Exception ex)
            {
                return string.Empty;
            }
            finally
            {
                AndroidJNI.PopLocalFrame(System.IntPtr.Zero);   //  全てのローカル参照から現在のローカル参照フレームを取り出し、resultオブジェクトを渡すために前のローカル参照フレームを返す
            }

            return pName;
        }
    }

    /// <summary>
    /// キャプチャ関連
    /// </summary>
    public class CaptureManager
    {
        /// <summary>
        /// スクリーンショットを撮影する
        /// </summary>
        /// <param name="p_Path">画像パス</param>
        /// <returns></returns>
        public static String MoveToAlbum(string p_Path)
        {
            if (string.IsNullOrEmpty(p_Path) || Application.isEditor) return "";

#if UNITY_ANDROID
            //  指定したパスのイメージファイルを取得する
            using (var captureImageFile = new AndroidJavaObject("java.io.File", p_Path))
            {
                //  ファイルが存在するか
                bool a_bExists = captureImageFile.Call<bool>("exists");
                if (!a_bExists) return "";

                //  出力先パス
                AndroidJavaObject a_ExternalPath = new AndroidJavaClass("android.os.Environment").CallStatic<AndroidJavaObject>("getExternalStorageDirectory");
                string a_OutputPath = a_ExternalPath.Call<string>("getAbsolutePath") + "/smartar/capture/";

                //  指定したファイルが存在しない時は新たに生成する
                a_bExists = new AndroidJavaObject("java.io.File", a_OutputPath).Call<bool>("exists");
                if (!a_bExists)
                {
                    new AndroidJavaObject("java.io.File", a_OutputPath).Call("mkdirs");
                }

                string a_ImageFileName = captureImageFile.Call<string>("getName");
                using (var destFile = new AndroidJavaObject("java.io.File", a_OutputPath, a_ImageFileName))
                {
                    bool a_bSuccess = captureImageFile.Call<bool>("renameTo", destFile);
                    return destFile.Call<string>("getAbsolutePath");
                }
            }
            return "";
#elif UNITY_IOS
            _WriteImageToAlbum(p_Path);
			return "";
#endif
        }

        [DllImport("__Internal")]
        //  指定した画像をカメラロールに書き込む
        private static extern void _WriteImageToAlbum(string p_Path);
    }

    /// <summary>
    /// シャッター音再生マネージャ
    /// </summary>
    public class ShutterManager
    {
        /// <summary>
        /// シャッター音を再生する
        /// </summary>
        public static void Play()
        {
            if (Application.isEditor) return;

#if UNITY_ANDROID
            using (AndroidJavaClass javaClass = new AndroidJavaClass("com.tryants.utils.SystemSoundManager"))
            {
                javaClass.CallStatic("playShutter");
            }
#elif UNITY_IOS
            _PlaySystemShutterSound();
#endif
        }

        [DllImport("__Internal")]
        //  シャッター音を再生する
        private static extern void _PlaySystemShutterSound();
    }

    /// <summary>
    /// バイブレートマネージャ
    /// </summary>
    public class VibratorManager
    {
        /// <summary>
        /// 指定した時間バイブレーションを実行（単位：ミリ秒　1000で１秒）
        /// iOSは時間指定ができないので1000ミリ以上指定で長めの振動　1000ミリ未満で短い振動
        /// </summary>
        /// <param name="p_time"></param>
        public static void On(long p_Time = 1000)
        {
            if (Application.isEditor) return;

#if UNITY_ANDROID
            using (AndroidJavaClass jc = UnityPlayerActivity)
            {
                using (AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (AndroidJavaObject vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator"))
                    {
                        vibrator.Call("vibrate", p_Time);
                    }
                }
            }
#elif UNITY_IOS
            //  iOSは振動の時間が指定できないのでUnity側の関数を使用する
			Handheld.Vibrate();
#endif
        }
    }

    public class ToastManager
    {
        /// <summary>
        /// 表示位置(画面に対しての)
        /// </summary>
        public enum eGravity
        {
            None = -1,

            Top,
            Center,
            Bottom
        }

        /// <summary>
        /// 表示時間
        /// </summary>
        public enum eLength
        {
            Short,
            Long
        }

        /// <summary>
        /// トーストを表示する
        /// </summary>
        /// <param name="p_Message">文言</param>
        /// <param name="p_Gravity">表示位置（画面に対して）</param>
        /// <param name="p_Length">表示時間</param>
        public static void  show(string p_Message, eGravity p_Gravity, eLength p_Length)
        {
            if (Application.isEditor) return;
#if !UNITY_ANDROID
            return;
#endif

            using (AndroidJavaClass jc = new AndroidJavaClass("com.tryants.utils.toastManager"))
            {
                jc.CallStatic("show", p_Message, (int)p_Gravity, (int)p_Length);
            }
        }
    }
}
